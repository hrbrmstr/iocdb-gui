Code for the IOCDB GUI Prototype

UI Framework: W2ui.  A FREE, opensource javascript framework that provides built in Ajax for saving and pulling data.  There are other frameworks available, but they cost between $400-$900 and may be needed based on future needs of the team.  
Documentation can be found at: http://w2ui.com/web/

Requirements:
A JavaScript/HTML that provides a local web server or you can use a text editor with a separate web server.

Current Functionality:
- Upload a file that contains free-form text and the IOCs will be parsed and added to the grid.  TO DO: Identify URLs w/:port
- Upload a formatted file (comma or tab delimited) and the content will be imported into the grid
- Copy and Paste data into the text area that fits the file upload options
- Grid is editable by cell.  A form is available, but there are issues with the save and the grid display
- Default values can be added for each column. TO DO: Remove the observables column

What it can't do:
- Send data to the dispatcher
- Create Associations
- Full error checking is not implemented
- Entering IOCs individually via the form
- Searching

Files:
iocdb.html:
The UI file that consists of HTML markup and Javascript code.

jquery.min.js:
JQuery Library that w2ui is based on.

main.js:
A catch-all file for general JavaScript variables, functions, and data.

papaparse.min.js:
A JQuery plugin that parses CSV -> JSON and JSON -> CSV.  Used primarily for importing data
into the grid.

w2ui-1.4.css:
W2ui style sheet

W2ui-1.4.js:
Main W2ui library

