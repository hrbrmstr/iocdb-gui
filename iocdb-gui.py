from flask import Flask
from flask import render_template
from flask import request
from datetime import datetime
from iocdb.model import Rumor, Document, Observable, TTP
import iocdb.dispatcher



app = Flask(__name__)

def get_rumors():
    #Set values
    valid = datetime.now()
    doc = Document(name='Test1', category='osint', source='Manual Test NUmber 1')
    obs = Observable('ip', '50.21.36.89')
    ttp = TTP(category='malicious host', actions='malware')
    desc = 'This is a description for Test 1'
    yield Rumor(obs, valid, doc, ttp, description=desc)


@app.route('/')
def iocdb_main():
    return render_template('iocdb_main.html')

@app.route('/rumors', methods=['POST'])
def rumors():
    if request.method == "POST":
        rec = request.values['changes'][0]
        return "{status: 'success'}"

if __name__ == '__main__':
    app.debug = True
    app.run()
