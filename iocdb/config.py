'''
handles parsing of config data
'''

#TODO: a better design would not involve side effects in the parse_ functions
#parse_ functions should return the values parsed rather than passing around
#a config object

import argparse
import logging
import logging.config
import os
import sys

import iocdb

MODULEMEMBERS = set(
    ('__builtins__', '__doc__', '__file__', '__name__', '__package__'))

class Configuration:

    def __init__(self, cls, kwargs):
        self.cls = cls
        self.kwargs = kwargs

    def make(self):
        obj = self.cls(**self.kwargs)
        return obj

def parse_config():
    '''
    initialize args from config modules in the following order
    1. default config (iocdb.default)
    2. local config found in data directory (iocdb.datadir)
    3. cwd config (iocdb_config module in cwd)
    4. argv config (--config)
    '''
    import iocdb.default # imported here to prevent cyclic dependency
    args = iocdb.default
    sys.path.append(iocdb.datadir)
    _update_with_local_config(args)
    sys.path.append(os.getcwd())
    _update_with_local_config(args)
    args, argv, parser = _update_with_argv_config(args)
    return args, argv, parser

def _update_with_local_config(args):
    try:
        local_config = _import_config('iocdb_config')
        _update_config(args, local_config)
    except ImportError:
        pass #do nothing, local config not found

def _import_config(path):
    __import__(path)
    return sys.modules[path]

def _update_config(args, new_config):
    if new_config is not None:
        new_attrs = set(dir(new_config)) - MODULEMEMBERS
        for attr in new_attrs:
            value = getattr(new_config, attr)
            setattr(args, attr, value)

def _update_with_argv_config(args):
    parser = argparse.ArgumentParser(add_help=False)
    group = parser.add_argument_group('config')
    group.add_argument('--config', help='path to config module',
                       metavar='IMPORTPATH', type=_import_config)
    args, argv = parser.parse_known_args(sys.argv, args)
    _update_config(args, args.config)
    return args, argv, parser

def parse_logging(argv, args):
    '''
    side effect: initialize dictConfig as args.logging
    and set logging.config.dictConfig
    '''
    parser = argparse.ArgumentParser(add_help=False)
    group = parser.add_argument_group('logging')
    group.add_argument('--loglevel',
                      choices=('DEBUG', 'INFO', 'WARNING', 'ERROR'))
    args, argv = parser.parse_known_args(argv, args)
    dictConfig = args.logging
    if args.loglevel is not None:
        if 'root' not in dictConfig:
            dictConfig['root'] = {}
        dictConfig['root']['level'] = args.loglevel
    logging.config.dictConfig(args.logging)
    return args, argv, parser

def parse_session_manager(argv, args, config_name, default):
    '''
    side effect: initialize session manager as args.config_name
    '''
    choices = getattr(args, config_name+'_choices')
    #TODO: break this out into functional parts
    #--- parse session_manager choice
    choice_parser = argparse.ArgumentParser(add_help=False)
    group = choice_parser.add_argument_group(
        '%s session manager' % config_name)
    group.add_argument(
        '--%s' % config_name, default=default,
        help='%s session manager key' % config_name,
        choices=choices.keys())
    args, argv = choice_parser.parse_known_args(argv, args)
    #--- parse session_manager kwargs
    kwarg_parsers = []
    for config in choices.values():
        if hasattr(config.cls, 'update_parser'):
            parser = argparse.ArgumentParser(add_help=False)
            group = parser.add_argument_group(
                '%s session manager' % config_name)
            kwarg_parsers.append(parser)
            config.cls.update_parser(group, config_name)
            session_manager_args, arv = parser.parse_known_args(argv)
            config.kwargs.update(vars(session_manager_args))
    #--- make session manager
    choice = getattr(args, config_name)
    session_manager = choices[choice].make()
    setattr(args, config_name, session_manager)
    #--- make help parser
    parser = argparse.ArgumentParser(
        add_help=False, parents=[choice_parser]+kwarg_parsers)
    return args, argv, parser

