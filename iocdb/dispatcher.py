import logging
import sys

import celery
from celery.utils.log import get_task_logger
#logging = get_task_logger(__name__)

import config
import feeds

#--- load config
args, argv, parser = config.parse_config()
args, argv, logging_parser = config.parse_logging(argv, args)
message_handlers = [config.make() for config in args.message_handlers]
#TODO: proper logging
print('initialized dispatcher with {}'.format(message_handlers))
page_size = 5000

#--- initialize app
include = [] 
include.extend('iocdb.feeds.'+feed for feed in feeds.__all__)
app = celery.Celery('iocdb-dispatcher', backend='amqp', include=include)
app.conf.CELERY_ROUTES = {
#    'iocdb.dispatcher._handle_message': {'queue': 'propagate'},
#    'iocdb.feeds.legacy._load_legacy': {'queue': 'legacy'},
    'iocdb.feeds.migrate._migrate_range': {'queue': 'migrate'}
}

#--- tasks
#def propagate(domain_objects, source=None, delay=True):
def propagate(domain_objects, source=None, delay=False):
    for page in _get_paged(domain_objects, source):
        if delay:
            celery.group(
                _handle_message.s(index, page, source)
                for index in range(len(message_handlers))
            )()
        else:
            for index in range(len(message_handlers)):
                _handle_message(index, page, source)

def _get_paged(domain_objects, source):
    page = []
    count = 0
    for obj in domain_objects:
        if len(page) == page_size:
            yield page
            page = []
        page.append(obj)
        count += 1
    yield page
    logging.info('propagating {} objects from {}'.format(
        count, source))

@app.task(ignore_results=True)
def _handle_message(index, domain_objects, source):
    message_handler = message_handlers[index]
    logging.info('handling {} objects from {}: {}'.format(
        len(domain_objects), source, message_handler))
    results = message_handler.handle_message(domain_objects)

if __name__ == '__main__':
    app.start()
