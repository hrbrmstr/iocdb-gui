import query_tools
import query_tools.json_encoder

import iocdb.model

class JSONEncoder(query_tools.JSONEncoder):
    
    def __init__(self):
        super(JSONEncoder, self).__init__(iocdb.model.Rumor)

    def make_session(self):
        return JSONEncoderSession(self.model_dict_mapper)

class JSONEncoderSession(query_tools.json_encoder.JSONSession):
    
    def add_all(self, model_objects):
        json_data = super(JSONEncoderSession, self).add_all(model_objects)
        print(json_data)
