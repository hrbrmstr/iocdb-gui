import json

import elasticsearch
import query_tools

import iocdb.model
import iocdb.mappings.dict
import iocdb.mappings.es

#TODO: this is all kind of a mess
#in the first place inheritance shoulnd't occure across packages
#in the second place most of this is a bandaid around missing 
#functionality in the query-tools package

ModelType_to_type_name = {
    iocdb.model.Rumor:'rumor', iocdb.model.Association:'association'}

ModelType_to_dict_schema = {
    iocdb.model.Rumor:iocdb.mappings.dict.dict_rumor_schema,
    iocdb.model.Association:iocdb.mappings.dict.dict_association_schema
}

class ElasticSearch(query_tools.ElasticSearch):

    def __init__(self, hosts=[{'host':'localhost'}]):
        super(ElasticSearch, self).__init__(
            'iocdb', ModelType_to_type_name, ModelType_to_dict_schema, hosts)

    def make_session(self):
        #TODO: this shouldn't be done with inheritance, leaks parent
        #implementation details
        session = ElasticSearchSession(
            self.index, self.ModelType_to_type_name,
            self.type_name_to_dict_mapper,
            self.ModelType_to_schema_mapper,
            self.hosts)
        return session

    def handle_message(self, domain_objects):
        results = None
        with self.make_session() as session:
            results = session.add_all(domain_objects)
            return results

    def setup(self, index='iocdb_default'):
        es = elasticsearch.Elasticsearch(hosts=self.hosts)
        #TODO: proper logging
        print('creating index {}:'.format(index))
        results = es.indices.create(index=index, body=iocdb.mappings.es.iocdb)
        print(results)
        #TODO: this looks like an es bug, it does not filter by alias name
        old_indexes = es.indices.get_aliases(name=['iocdb'])
        old_indexes = [i for i, v in old_indexes.items()
                       if 'iocdb' in v['aliases']]
        if len(old_indexes) > 1:
            print('WARNING: iocdb has multiple indexes. must update aliases manually')
            return
        actions = [{'add': {'index':index, 'alias':'iocdb'}}]
        if len(old_indexes) == 1:
            actions.append(
                {'remove': {'index':old_indexes[0], 'alias':'iocdb'}})
        print('updating aliases: {}'.format(actions))
        results = es.indices.update_aliases(body={'actions':actions})
        print(results)

class ElasticSearchSession(query_tools.es.ElasticSearchSession):

    def add_all(self, domain_objects):
        '''es doesn't like the null ids that can be found in the iocdb model'''
        results = super(ElasticSearchSession, self).add_all(
            self._del_null_id(domain_objects))
        return results

    def query(self, ModelType, criteria):
        '''edit the path of multimapped fields'''
        #TODO: querytools.es should have a copy of the mapping to do this
        #automatically
        _not_analyzed = {
            ('observable','value'):('observable','value','untouched'),
            ('document','name'):('document','name','untouched')
        }
        subcriteria = self._get_subcriteria(criteria)
        for subc in subcriteria:
            if subc.path in _not_analyzed:
                subc.path = _not_analyzed[subc.path]
        obj_iter = super(ElasticSearchSession, self).query(
            ModelType, criteria)
        for obj in obj_iter:
            yield obj

    @staticmethod
    def _get_subcriteria(criteria):
        subcriteria = []
        if hasattr(criteria, 'conjunction'):
            for sub in criteria:
               subcriteria.extend(ElasticSearchSession._get_subcriteria(sub))
        else:
            subcriteria.append(criteria)
        return subcriteria
        
    @staticmethod
    def _del_null_id(domain_objects):
        #TODO: I can't explain why this is not working, fixing downstream in the json
        for obj in domain_objects:
            if obj.id is None:
                del obj.id
            yield obj
