import os
import os.path

import query_tools
import sqlalchemy

import iocdb
import iocdb.mappings.table

class SQLAlchemy(query_tools.SQLAlchemy):

    def __init__(self, engine_url):
        super(SQLAlchemy, self).__init__(
            iocdb.mappings.table.sql_metadata, 
            {iocdb.mappings.table.aggregate_rumor_schema:
                 iocdb.mappings.table.rumors_aggregate_table},
            engine_url)

    def setup(self):
        iocdb.mappings.table.aggregate_metadata.create_all(self.engine)
