import argparse

import query_tools

import iocdb.mappings.table

class CSVEncoder(query_tools.CSVEncoder):

    def __init__(self, csv_columns):
        super(CSVEncoder, self).__init__(
            iocdb.mappings.table.rumors_aggregate_mapping,
            fieldnames=csv_columns)

    @staticmethod
    def update_parser(group, group_name=None):
        group.add_argument('--csv_columns', nargs='*', metavar='COLUMNNAME')
