import os

import query_tools

import iocdb
import iocdb.mappings.table

DEFAULT_FILE_PATH = os.path.join(iocdb.datadir, 'ioc.db')

class SQLite(query_tools.SQLite):

    def __init__(self, db_file_path=DEFAULT_FILE_PATH):
        super(SQLite, self).__init__(
            iocdb.mappings.table.sql_metadata, 
            {iocdb.mappings.table.aggregate_rumor_schema:
                 iocdb.mappings.table.rumors_aggregate_table},
            iocdb.mappings.table.rumors_aggregate_mapping,
            db_file_path)

    def handle_message(self, domain_objects):
        results = None
        with self.make_session() as session:
            results = session.add_all(domain_objects)
        return results

    def setup(self):
        self.sqla_metadata.create_all(self.engine)
