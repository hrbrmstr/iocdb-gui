'''
default configuration parameters
'''

import os.path

import iocdb
import iocdb.config
import iocdb.session_managers.csv_encoder
import iocdb.session_managers.es
import iocdb.session_managers.json_encoder
import iocdb.session_managers.stix_encoder
import iocdb.session_managers.sqlite
import iocdb.session_managers.sqla

logging = {'version':1}

sqlite_config = iocdb.config.Configuration(
    iocdb.session_managers.sqlite.SQLite, {})
sqla_config = iocdb.config.Configuration(
    iocdb.session_managers.sqla.SQLAlchemy,
    {'engine_url':'sqlite:///'+iocdb.session_managers.sqlite.DEFAULT_FILE_PATH})
csv_config = iocdb.config.Configuration(
    iocdb.session_managers.csv_encoder.CSVEncoder, {})
json_config = iocdb.config.Configuration(
    iocdb.session_managers.json_encoder.JSONEncoder, {})
stix_config = iocdb.config.Configuration(
    iocdb.session_managers.stix_encoder.STIXEncoder, {})
elasticsearch_config = iocdb.config.Configuration(
    iocdb.session_managers.es.ElasticSearch, {})

message_handlers = [sqlite_config]

encoder_choices = {
    'json':json_config,
    'csv':csv_config,
    'stix':stix_config,
    'sqlite':sqlite_config,
    'sqla':sqla_config,
    'elasticsearch':elasticsearch_config
}

repository_choices = {
    'sqlite':sqlite_config,
    'sqla':sqla_config
}

