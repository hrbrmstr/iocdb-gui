'''a mapping from a dict to the data model'''

import mapping_tools

import iocdb.model

dict_observable_schema = mapping_tools.DictSchema(iocdb.model.Observable)
dict_document_schema = mapping_tools.DictSchema(iocdb.model.Document)
dict_ttp_schema = mapping_tools.DictSchema(iocdb.model.TTP)
dict_actor_schema = mapping_tools.DictSchema(iocdb.model.Actor)
dict_campaign_schema = mapping_tools.DictSchema(iocdb.model.Campaign)

dict_rumor_schema = mapping_tools.DictSchema(iocdb.model.Rumor, {
    'observable': dict_observable_schema,
    'document': dict_document_schema,
    'ttp': dict_ttp_schema,
    'actor': dict_actor_schema,
    'campaign': dict_campaign_schema
})

dict_association_schema = mapping_tools.DictSchema(iocdb.model.Association, {
    'left': dict_observable_schema,
    'right': dict_observable_schema,
    'document': dict_document_schema
})

