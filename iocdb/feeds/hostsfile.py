#!/usr/bin/env python

import re
import json
import urllib2
from datetime import datetime

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

ignore = re.compile('^#')

ttp = TTP(category="malicious host")
desc = 'Hosts File Listing'
feedurl = "http://hosts-file.net/download/hosts.txt"

@periodic_task(run_every=crontab(minute=0, hour=4), ignore_result=True)
def parse():
    page = urllib2.urlopen(feedurl)
    valid = _parse_header(page)
    doc = Document(name=feedurl, category='osint', source='Hosts File Sites')
    iocdb.dispatcher.propagate(_get_rumors(page, valid, doc), __file__)

def _parse_header(page):
    header = page.readlines(1)
    day, month, year= header[0].split('\t')[2].split(' ')[0].split('/')
    valid = datetime(int(year),int(month),int(day))
    return valid
                    
def _get_rumors(page, valid, doc):
    for line in page.readlines()[26::]:
        if not ignore.match(line):
            fields = line.split()
            obs = Observable('domain', fields[1])
            yield Rumor(obs, valid, doc, ttp, description=desc)
