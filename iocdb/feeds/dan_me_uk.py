#!/usr/bin/env python

from urllib import urlopen
from datetime import datetime

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

ttp = TTP(category="proxy")
feed = "https://www.dan.me.uk/tornodes"

@periodic_task(run_every=crontab(minute=40), ignore_result=True)
def parse():
    valid = datetime.now()
    doc = Document(name=feed, category='osint', source='dan.me.uk TOR Node List')
    page = urlopen(feed).read()
    tor_nodes = _parse_tor_nodes(page)
    iocdb.dispatcher.propagate(_get_rumors(valid, tor_nodes, doc), __file__)

def _parse_tor_nodes(page):
    start_content = page.find('<!-- __BEGIN_TOR_NODE_LIST__ //-->')+35
    end_content = page.find('<!-- __END_TOR_NODE_LIST__ //-->')-1
    content = page[start_content:end_content]
    nodes = [node.split('|') for node in content.split('<br />\n')]
    return nodes

def _get_rumors(valid, tor_nodes, doc):
    for node in tor_nodes:
        obs = Observable('ip', node[0])
        desc = 'Tor {} hostname: {} ORport: {} DirPort: {}'.format(
            'exit node' if 'E' in node[4] else 'server', *node[1:4])
        yield Rumor(obs, valid, doc, ttp, description=desc)

