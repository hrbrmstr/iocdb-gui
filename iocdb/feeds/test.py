#!/usr/bin/env python


from datetime import datetime

from iocdb.model import Rumor, Document, Observable, TTP
import iocdb.dispatcher

def get_rumors():
    #Set values
    valid = datetime.now()
    doc = Document(name='Test1', category='osint', source='Manual Test NUmber 1')
    obs = Observable('ip', '50.21.36.89')
    ttp = TTP(category='malicious host', actions='malware')
    desc = 'This is a description for Test 1'
    yield Rumor(obs, valid, doc, ttp, description=desc)

#send it to the dispatcher
iocdb.dispatcher.propagate(get_rumors())
