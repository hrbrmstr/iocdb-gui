'''
implements relational table bindings for Model.

### Relation columns barrow from sqlalchemy:
```
>>> class Geometry(Model, Relation):
...     plural = 'geometries'
...     sides = Member(Column(Integer))
...     name = Member(Column(String(50)))
...     def __init__(self, sides, name):
...         self.sides = sides
...         self.name = name
...     def __repr__(self):
...         return '<The %s has %i sides.>' % (self.name, self.sides)
...
>>> square = Geometry(sides=4, name=u'square')
>>> octagon = Geometry(sides=8, name=u'octagon')
>>> rectangle = Geometry(sides=4, name=u'rectangle')
>>> engine = RelationEncoding.setup(drivername='sqlite')
>>> session = RelationEncoding(engine)
>>> session.dump({'geometries':[square, octagon, rectangle]})
>>> session.commit()

```
### sessions return a query wrapper object for SQLA query object
```
>>> query = session.query(Geometry)
>>> session.load(query).next() # doctest: +NORMALIZE_WHITESPACE
{'geometries': [<The square has 4 sides.>, <The octagon has 8 sides.>,
                <The rectangle has 4 sides.>]}
>>> query = session.query(Geometry).in_('name', [u'square', u'octagon'])
>>> session.load(query).next() # doctest: +NORMALIZE_WHITESPACE
{'geometries': [<The square has 4 sides.>, <The octagon has 8 sides.>]}

```
### A relation with a many to one join:
```
>>> class Goose(Model, Relation):
...     plural = 'geeses'
...     name = Member(Column(String(50)))
...     def __init__(self, name):
...         self.name = name
...     def __repr__(self):
...         return '<%s is a goose>' % self.name
...
>>> class Penguin(Model, Relation):
...     plural = 'penguins'
...     name = Member(Column(String(50)))
...     mother = Member(ManyToOne, model='Goose')
...     def __init__(self, name, mother):
...         self.name = name
...         self.mother = mother
...     def __repr__(self):
...         return '<%s is the child of %s>' % (self.name, self.mother.name)
...
>>> betty = Goose(name=u'betty')
>>> lucy = Goose(name=u'lucy')
>>> fred = Penguin(name=u'fred', mother=betty)
>>> jeff = Penguin(name=u'jeff', mother=lucy)
>>> engine = RelationEncoding.setup(drivername='sqlite')
>>> session = RelationEncoding(engine)
>>> session.dump({'penguins':[fred, jeff]})
>>> session.commit()
>>> query = session.query(Penguin)
>>> session.load(query).next() # doctest: +NORMALIZE_WHITESPACE
{'penguins': [<fred is the child of betty>, <jeff is the child of lucy>]}

```
'''

#TODO: suck less at sqla. maybe get rid of declarative base and use tables

from collections import defaultdict
from model import Model, Member, ModelRegister
from encoding import Encoder

def load_tests(loader, tests, pattern):

    from doctest import DocTestSuite
    from unittest import TestSuite
    suite = TestSuite()
    suite.addTests(DocTestSuite())
    return suite

class NullMember(object):
    '''null members allow models to define relation bindings even if relations
       aren't implemented'''
    def __init__(self, *args, **kwargs):
        pass #do nothing

#TODO: handle import errors on the other side of the dependency
Relation = object
RelationEncoding = None
Integer = NullMember
String = NullMember
Boolean = NullMember
Text = NullMember
DateTime = NullMember
Enum = NullMember
Column = NullMember
ManyToOne = NullMember
Boolean = NullMember

try:
    import sqlalchemy
    from sqlalchemy.ext.declarative import declarative_base, declared_attr
    from sqlalchemy import Sequence
    from sqlalchemy.schema import UniqueConstraint, ForeignKey, Index
    from sqlalchemy.orm import relationship, aliased
    from sqlalchemy import Column, Integer, DateTime, Enum, Boolean
    from sqlalchemy import Unicode as String
    from sqlalchemy import UnicodeText as Text
    from sqlalchemy.types import TypeDecorator, Unicode

    #TODO: is this doing anything?
    class CoerceUTF8(TypeDecorator):
        """Safely coerce Python bytestrings to Unicode
        before passing off to the database."""

        impl = Unicode

        def process_bind_param(self, value, dialect):
            if isinstance(value, str):
                value = value.decode('utf-8')
            return value

    class ManyToOne(object):

        def __init__(self, model, nullable=None, index=True):

            self.model = model.lower()

            self.fkargs = {}
            if not nullable is None:
                self.fkargs['nullable'] = nullable
            if index:
                self.fkargs['index'] = True

    class Base(object):
        @declared_attr
        def __tablename__(cls):
            return cls.plural #TODO: redundant?

        @declared_attr
        def uid(cls):
            return Column(Integer, Sequence('%s_uid_seq'%cls.__tablename__),
                          primary_key=True)

    class BaseFactory(ModelRegister):
        def __init__(cls, name, bases, namespace):

            if name == 'Relation': #initialize factory
                cls._pendingmanytoone = defaultdict(lambda: [])

            else: #construct SQLA Base
                super(BaseFactory, cls).__init__(name, bases, namespace)

                #each version of the model needs its own metadata
                register = Model.get_version(cls.version)
                if not hasattr(register, 'Base'):
                    register.Base = declarative_base(cls=Base)

                d = {}
                d['__tablename__'] = cls.plural #TODO: redundant?
                d['_Model'] = cls

                columns = cls.get_members(Column)
                manytoones = cls.get_members(ManyToOne)

                for member_name, column in columns:
                    d[member_name] = column

                if hasattr(cls, 'unique'):
                    ucols = []
                    for member_name in cls.unique:
                        if member_name in [mn for mn, col in columns]:
                            ucols.append(member_name)
                        elif member_name in [mn for mn, mto in manytoones]:
                            ucols.append('%s_uid' % member_name)

                    indexname = 'unique_%s' % d['__tablename__']
                    d['unique'] = UniqueConstraint(*ucols, name=indexname)

                cls._Base = type(name, (register.Base,), d)

                for member_name, manytoone in manytoones:

                    #if MemberModel already registered
                    if manytoone.model in Model:
                        BaseFactory._buildmanytoone(
                            cls._Base, Model[manytoone.model]._Base,
                            member_name, manytoone)

                    else: #create relation when MemberModel is registered
                        cls._pendingmanytoone[manytoone.model].append(
                            (cls._Base, member_name, manytoone))

                #handle pending relations
                if cls.modelname in cls._pendingmanytoone:
                    for Parent, member_name, manytoone\
                        in cls._pendingmanytoone[cls.modelname]:
                            BaseFactory._buildmanytoone(
                                Parent, cls._Base, member_name, manytoone)

        @staticmethod
        def _buildmanytoone(Parent, Child, member_name, manytoone):
            fk = Column(Integer,
                        ForeignKey('%s.uid' % Child.__tablename__),
                        **manytoone.fkargs)
            setattr(Parent, '%s_uid' % member_name, fk)
            setattr(Parent, member_name, relationship(Child, foreign_keys=fk))

    class Relation(object):
        '''Relational table Model Binding'''

        __metaclass__ = BaseFactory

        def __new__(cls, *args, **kwargs):
            '''initialize base object'''

            cls = super(Relation, cls).__new__(cls, *args, **kwargs)
            cls._base = cls._Base()

            return cls

        def __setattr__(self, name, value):
            try:
                #set value of base
                if hasattr(self._base, name):
                    if isinstance(value, Model):
                        setattr(self._base, name, value._base)
                    else:
                        setattr(self._base, name, value)
            except AttributeError: #setting _base attribute
                pass

            object.__setattr__(self, name, value)

        def dump_row(self, session):
            '''check if row in db and merge if it is'''

#            if hasattr(self, 'unique'):
#
#                #query
#                identity = [(mn, getattr(self, mn)) for mn in self.unique]
#                query = session.query(self._Base)
#                for member_name, value in identity:
#                    column = getattr(self._base, member_name)
#                    query = query.filter(column == value)
#                obj = query.first()
#
#                #merge
#                if obj:
#                    self._base = obj

            #TODO: ?
            for member_name, manytoone in self.__class__.get_members(ManyToOne):
                member = getattr(self, member_name)
                if member:
                    try:
                        member.dump_row(session)
                    except AttributeError:
                        raise AttributeError(
                            '%s many to one member %s has no attribute ' \
                            % (self.modelname, member_name) +\
                            'dump_row. member of type %s expected but %s ' \
                            % (manytoone.model, type(member).__name__) +\
                            'was found.')

            return self._base

        @classmethod
        def load_row(cls, row):
            '''initialize a model from the row SQLA base'''

            modelargs = {}
            for member_name, column in cls.get_members(Column):
                value = getattr(row, member_name)
                modelargs[member_name] = value

            for member_name, manytoone in cls.get_members(ManyToOne):
                relatedbase = getattr(row, member_name)
                if relatedbase is not None:
                    modelargs[member_name] = \
                        relatedbase._Model.load_row(relatedbase)

            obj = cls(**modelargs)
            obj._base = row

            return obj

    class Query:
        '''relation encoder helper class wraps SQLA query
           TODO: why?
        '''

        def __init__(self, session, QueryModel, page_size=100000):
            self.session = session
            self.QueryModel = QueryModel
            self._query = session.query(self.QueryModel._Base)
            self.maxlimit = None
            self.page_size = page_size

        def in_(self, member_name, value):
            col = self._getcol(member_name)
            self._query = self._query.filter(col.in_(value))
            return self

        def notin(self, member_name, value):
            col = self._getcol(member_name)
            self._query = self._query.filter(~ col.in_(value))
            return self

        def gte(self, member_name, value):
            col = self._getcol(member_name)
            self._query = self._query.filter(col >= value)
            return self

        def lt(self, member_name, value):
            col = self._getcol(member_name)
            self._query = self._query.filter(col < value)
            return self

        def _getcol(self, member_name):
            col = getattr(self.QueryModel._Base, member_name)
            return col

        def subquery(self):
            alias = aliased(self.QueryModel._Base, self._query.subquery())
            return alias

        def join(self, alias, member_name):
            col = self._getcol(member_name)
            self._query = self._query.join(alias, col)
            return self

        def limit(self, limit):
            #self._query = self._query.limit(limit)
            self.maxlimit = limit

        def union_all(self, union):
            self._query = self._query.union_all(union._query)
            return self

        def __iter__(self):
            return iter(self._query)

    import logging
    class RelationEncoding(Encoder):

        def __init__(self, engine=None, **dbconfig):

            if not engine:
                engine = self.setup(**dbconfig)
            self.version = engine.version
            self.session = self._setupsession(engine)

            super(RelationEncoding, self).__init__('dump_row', self.session)

        @staticmethod
        def _setupengine(connect_args=None, poolclass=None, **dbconfig):

            if len(dbconfig) == 0:
                dbconfig['drivername']='sqlite'

            createargs = {}
            if connect_args:
                createargs['connect_args'] = connect_args
            if poolclass:
                createargs['poolclass'] = poolclass

            #TODO: there appears to be a sqla bug in parsing sqlite hosts
            if dbconfig['drivername'] == 'sqlite' and 'host' in dbconfig:
                url = 'sqlite://%s' % dbconfig['host']
                engine = sqlalchemy.create_engine(url)
            else:
                url = sqlalchemy.engine.url.URL(**dbconfig)
                engine = sqlalchemy.create_engine(url, **createargs)

            return engine

        @staticmethod
        def _setupsession(engine):

            session = sqlalchemy.orm.sessionmaker(bind=engine)()
            return session

        def dump(self, data):
            #TODO: test unique here with a join, not with individual queries

            data = super(RelationEncoding, self).dump(data)
            if len(data) == 0:
                return #do nothing
            version = None
            if 'version' in data:
                version = data['version']
                del(data['version'])
            if not version == self.version:
                raise ValueError(
                    'data version %s does not match db version %s' \
                    % (version, self.version))
            for k, v in data.items():
                if isinstance(v, list):
                    for obj in v:
                        if hasattr(obj, 'uid'):
                            self.session.merge(obj)
                        else:
                            self.session.add(obj)
                else: #TODO: is this ever true?
                    if hasattr(obj, 'uid'):
                        obj._base = self.session.merge(obj._base)
                    else:
                      self.session.add(v)

        def query(self, QueryModel, **kwargs):
            '''query factory for this session'''
            return Query(self.session, QueryModel, **kwargs)

        def _paged_query(self, query):
            '''
            limit/lastid based paging
            TODO: not compatable with order by
            TODO: window fn paging
            http://www.sqlalchemy.org/trac/wiki/UsageRecipes/WindowedRangeQuery
            TODO: alternate solution: auto-updating page index member
            '''

            page_size = query.page_size
            record_count = 0
            records = []
            lastid = None
            orderedq = query._query.order_by(
                              query.QueryModel._Base.id.desc())
            while True:
                if query.maxlimit:
                    remaining = query.maxlimit - record_count
                    if not remaining:
                        break #reached record limit
                    if remaining < page_size: #last page
                        page_size = remaining
                        
                q = orderedq
                if lastid:
                    q = q.filter(query.QueryModel._Base.id < lastid)
                q = q.limit(page_size)
                page = self._load_rows(q, query.QueryModel)
                records = page[query.QueryModel.plural]
                if records:
                    lastid = records[-1]._base.id
                    logging.debug('load:last id %i' % lastid)
                else: #no records returned, no more data
                    break
                record_count += len(records)
                yield page

        def load(self, query):
            '''given an sqla like query, yield model objects'''

            #TODO: base should hasa model instance (not just a model class)
            #to prevent a cache check when initializing a model
            #that already exists

            if query.page_size > 0:
                for page in self._paged_query(query):
                    yield page

            else: 
                query._query = query._query.limit(query.maxlimit)
                yield self._load_rows(query)
            #print('load time %s' % loadtime.secs)

        def _load_rows(self, query, QueryModel=None):
            if not QueryModel:
                QueryModel = query.QueryModel
            modelobjs = []
            for row in query:
                modelobjs.append(row._Model.load_row(row))

            return {QueryModel.plural: modelobjs}

        def commit(self):
            self.session.commit()

        #rename create_engine
        @staticmethod
        def setup(version=None, **dbconfig):
            '''returns engine'''

            engine = RelationEncoding._setupengine(**dbconfig)
            engine.version = version
            Model.get_version(version).Base.metadata.create_all(engine)
            #TODO:
            #raise NotImplementedError(
            #    'dbapi not installed for engine %s' % engine)

            return engine

        def close(self):
            self.session.close()

        #def is_connected(self):
        #    result = self.session.execute("select 1").scalar()
        #    return result == 1

except ImportError:
    #relation encoding not implemented
    pass #implements null members
