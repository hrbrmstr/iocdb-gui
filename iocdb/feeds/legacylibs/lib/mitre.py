'''
a wrapper around stix xml bindings.
implements encoder to serialize generic model as STIX.

TODO: doctest
'''

from encoding import Encoder

def load_tests(loader, tests, pattern):
    from doctest import DocTestSuite
    from unittest import TestSuite
    suite = TestSuite()
    suite.addTests(DocTestSuite())
    return suite

STIXEncoder = None
STIXPersonnelWrapper = None
STIXStringObjectPropertyTypeWrapper = None

try:
    from stix.bindings.stix_core import STIXType, STIXHeaderType
    from stix.bindings.stix_core import DEFAULT_XML_NS_MAP
    from stix.bindings.stix_core import IndicatorsType, ThreatActorsType
    from stix.bindings.stix_common import StructuredTextType
    from stix.bindings.indicator import ValidTimeType
    from stix.bindings.ttp import TTPType, BehaviorType, ResourceType
    from stix.bindings.ttp import MalwareType, MalwareInstanceType
    from stix.bindings.ttp import InfrastructureType
    from stix.bindings.threat_actor import ObservedTTPsType, ThreatActorType
    from stix.bindings.data_marking import MarkingType
    from stix.bindings.data_marking import MarkingSpecificationType
    from stix.bindings.extensions.marking.tlp import TLPMarkingStructureType
    from stix.indicator import Indicator
    from stix.common import InformationSource, Identity
    #from stix.bindings.data_markings import MarkingType, MarkingStructureType
    #from stix.bindings.ttp
    #from stix.bindings.threat_actor
    #from stix.bindings.campaign
    import cybox
    from cybox.bindings.cybox_core import ObservablesType
    from cybox.bindings.cybox_common import PersonnelType
    from cybox.bindings.cybox_common import StringObjectPropertyType
    from cybox.core import Observable, Observables
    from cybox.common import Hash, Time, MeasureSource, Personnel, Contributor
    from cybox.objects.file_object import File
    from cybox.objects.uri_object import URI
    from cybox.objects.address_object import Address

    class STIXEncoder:
        '''
        '''

        def __init__(self, fp):
            self.fp = fp

        def dump(self, data):
            package = STIXType(Indicators=IndicatorsType(),
                               Observables=ObservablesType(),
                               STIX_Header=STIXHeaderType(
                                   Handling=MarkingType()))
            Encoder('dump_stix', package).dump(data)
            package.export(self.fp, 0, DEFAULT_XML_NS_MAP)

        def commit(self):
            pass #do nothing


    class STIXPersonnelTypeWrapper(PersonnelType):

        def export(self, outfile, level, nsmap, namespace_,
                   name_, pretty_print):
            return super(STIXPersonnelTypeWrapper, self).export(
                outfile=outfile, level=level, namespace_=namespace_,
                name_=name_, pretty_print=pretty_print)

    class STIXPersonnelWrapper(Personnel):
        _binding_class = STIXPersonnelTypeWrapper

    class STIXStringObjectPropertyTypeWrapper(StringObjectPropertyType):

        def export(self, outfile, level, nsmap, namespace_,
                   name_, pretty_print):
            return super(STIXStringObjectPropertyTypeWrapper, self).export(
                outfile=outfile, level=level, namespace_=namespace_,
                name_=name_, pretty_print=pretty_print)

except ImportError:
    pass #stix bindings not installed
