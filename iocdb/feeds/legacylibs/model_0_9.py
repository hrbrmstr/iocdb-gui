''' iocdb data model v0.9

## data model usage examples
define some source documents:
```
>>> govdoc = Document('govindicators05162013.csv', category='govint',\
                      source='myfeedname', tlp='amber', noforn=True)
>>> nslookup = Document(source='nslookup')
>>> whois = Document(source='whois')

```
define some observables:
```
>>> badip = Observable('ip', '1.1.1.1')
>>> exampledomain = Observable('domain', 'example.com')
>>> exampleip = Observable('ip', '192.0.43.10')
>>> exampleasn = Observable('asn', '26711')

```
define a threat:
```
>>> ttp = TTP(category='exploit server')

```
relate an observable to a threat:
```
>>> iprumor = Rumor(observable=badip, document=govdoc,\
                    ttp=ttp, valid=datetime(2013, 05, 16),\
                    description='some unstructured info about this relation')

```
associate two observables:
```
>>> resolution_assoc = Association(exampledomain, exampleip,\
                                   variety='resolves to',\
                                   valid=datetime(2013, 05, 16),\
                                   document=nslookup)
>>> assignment_assoc = Association(exampleip, exampleasn,\
                                   variety='assigned to',\
                                   valid=datetime(2013, 05, 16),\
                                   document=whois)

```
encode/decode model as json:
```
>>> from StringIO import StringIO
>>> fp = StringIO()
>>> from lib.encoding import JSONEncoder, JSONDecoder
>>> JSONEncoder(fp, indent=2).dump(\
        {'rumors':[iprumor],\
         'associations':[resolution_assoc, assignment_assoc]})
>>> print(fp.getvalue()) # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
{
  "associations": [
    {
      "left": {
        "value": "example.com",
        "variety": "domain"
      },
      "document": {
        "source": "nslookup",
        "tlp": "white",
        "name": "nslookup",
        "received": "..."
      },
      "right": {
        "value": "192.0.43.10",
        "variety": "ip"
      },
      "valid": "2013-05-16T00:00:00",
      "variety": "resolves to"
    },
    {
      "left": {
        "value": "192.0.43.10",
        "variety": "ip"
      },
      "document": {
        "source": "whois",
        "tlp": "white",
        "name": "whois",
        "received": "..."
      },
      "right": {
        "value": "26711",
        "variety": "asn"
      },
      "valid": "2013-05-16T00:00:00",
      "variety": "assigned to"
    }
  ],
  "version": "0_9",
  "rumors": [
    {
      "observable": {
        "value": "1.1.1.1",
        "variety": "ip"
      },
      "document": {
        "category": "govint",
        "received": "...",
        "tlp": "amber",
        "source": "myfeedname",
        "noforn":true,
        "name": "govindicators05162013.csv"
      },
      "ttp": {
        "category": "exploit server"
      },
      "description": "some unstructured info about this relation",
      "valid": "2013-05-16T00:00:00"
    }
  ]
}
>>> fp.seek(0)
>>> JSONDecoder(fp).load().next() # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
{u'associations': [<....Association object at ...>,
                   <....Association object at ...>],
 u'rumors': [<....Rumor object at ...>]}

```
'''

#TODO: implement a relation member binding to handle text fields that
#are an alias of an indexed char field

from datetime import datetime

from lib.model import ModelRegister, Model, Member
from lib.relation import Relation, RelationEncoding, ManyToOne
from lib.relation import Column, Integer, String, Text, DateTime, Enum
from lib.relation import Boolean
import lib.mitre as mitre

def load_tests(loader, tests, pattern):
    from doctest import DocTestSuite
    from unittest import TestSuite
    suite = TestSuite()
    suite.addTests(DocTestSuite())
    return suite

class Model(Model):
    version = '0_9'

TLP = set(('red','green','amber','white'))
OBSERVABLE_VARIETY = set(('ip','network','asn','url','email',
                      'domain','sha256','sha1','md5'))
TTP_CATEGORY = set(('malicious host', 'bot', 'exfiltration site', 'c2',
                'proxy', 'malware distributor', 'exploit server',
                'malware', 'sinkhole', 'malicious email address'))
DOCUMENT_CATEGORY = set(('ciscp', 'csint', 'govint', 'ids2', 'osint', 'vecirt',
                     'vzir', 'derived', 'atims', 'mss'))

class Rumor(Model, Relation):
    '''a rumor relates an observable to a threat.'''

    plural = 'rumors'
    unique = ('observable', 'valid', 'document', 'ttp', 'actor', 'campaign')

    #TODO: observable, document backreferences
    observable = Member(ManyToOne, model='Observable', nullable=False)
    valid = Member(Column(DateTime, nullable=False, index=True))
    document = Member(ManyToOne, model='Document', nullable=False)
    ttp = Member(ManyToOne, model='TTP')
    actor = Member(ManyToOne, model='Actor')
    campaign = Member(ManyToOne, model='Campaign')
    description = Member(Column(Text))

    def __init__(self, observable, valid, document, ttp=None, actor=None,
                 campaign=None, description=None):

        if not (ttp or actor or campaign):
            raise AttributeError(
                'a rumor must have at least one threat ' +
                '(ttp, actor or campaign)')

        if ttp:
            if observable.variety in ('md5', 'sha256', 'sha1')\
                and not ttp.category == 'malware':
                    raise AttributeError(
                        'file observables can only be related to ttp '+\
                        'category "malware"')

            if ttp.category == 'malware'\
                and not observable.variety in ('md5', 'sha256', 'sha1'):
                    raise AttributeError(
                        'ttp category malware can only be related to '+\
                        'file observables')

        #TODO: just take a datetime
        if isinstance(valid, str):
            self.valid = datetime.strptime(valid, '%Y-%m-%dT%H:%M:%S')
        else: #isinstance(valid, datetime)
            self.valid = valid

        self.observable = observable
        self.ttp = ttp
        self.actor = actor
        self.campaign = campaign
        self.document= document
        self.description = description

    def dump_stix(self, package):

        producer = self.document.dump_stix()
        observable = self.observable.dump_stix()
        indicator = mitre.Indicator(observables=[observable],
                                    description=self.description).to_obj()
        indicator.set_Producer(producer)
        indicator.add_Valid_Time_Position(
            mitre.ValidTimeType(Start_Time=self.valid))
        ttp = None
        if self.ttp:
            ttp = self.ttp.dump_stix()
            indicator.add_Indicated_TTP(ttp)
        if self.actor:
            annonttp = None
            if not ttp: #actor related to indicator via ttp
                annonttp = mitre.TTPType(idref=mitre.Identity().id_)
                indicator.add_Indicated_TTP(annonttp)
            obsttp = mitre.ObservedTTPsType(Observed_TTP=[annonttp])
            actorid = mitre.Identity(name=self.actor.name).to_obj()
            actor = mitre.ThreatActorType(Identity=actorid,
                                          Observed_TTPs=obsttp)
            if not package.get_Threat_Actors():
                package.set_Threat_Actors(mitre.ThreatActorsType())
            package.get_Threat_Actors().add_Threat_Actor(actor)
        if self.campaign:
            #TODO: campaing -> Campaign
            raise NotImplementedError('TODO')

        tlp = mitre.TLPMarkingStructureType(color=self.document.tlp)
        handling = mitre.MarkingType(
                       [mitre.MarkingSpecificationType(
                           Controlled_Structure='//node()',
                           Marking_Structure=[tlp])])
        indicator.set_Handling(handling)

        package.get_Indicators().add_Indicator(indicator)

class Association(Model, Relation):
    '''an associates relates two observables'''

    plural = 'associations'
    unique = ('left', 'right', 'valid', 'document')

    #TODO: observable, document backreferences
    left = Member(ManyToOne, nullable=False, model='Observable')
    right = Member(ManyToOne, nullable=False, model='Observable')
    valid = Member(Column(DateTime, nullable=False, index=True))
    variety = Member(Column(String(50), index=True))
    description = Member(Column(Text))
    document = Member(ManyToOne, nullable=False, model='Document')

    def __init__(self, left, right, valid, document, variety=None,
                 description=None):

        #parse datetime if isinstance(valid, str)
        if isinstance(valid, str):
            self.valid = datetime.strptime(valid, '%Y-%m-%dT%H:%M:%S')
        else: #isinstance(valid, datetime)
            self.valid = valid

        self.left = left
        self.right = right
        self.document = document
        self.variety = variety
        self.description = description

    def dump_stix(self, package):
        #TODO: map self.variety
        obs = self.left.dump_stix()
        relation = None
        if self.variety == 'resolves to':
            relation = 'Resolved_To'
        elif self.variety == 'assigned to':
            relation = 'Contained_Within'
        #TODO: handle other variety -> relation mappings
        obs.object_.add_related(self.right.dump_stix().object_.properties,
                                relation)
        obs.description = self.description
        obs = obs.to_obj()
        source = self.document.dump_cybox()
        source.get_Time().set_Start_Time(self.valid)
        obs.set_Observable_Source(source)

        tlp = mitre.TLPMarkingStructureType(color=self.document.tlp)
        package.get_STIX_Header().get_Handling().add_Marking(
            mitre.MarkingSpecificationType(
                Controlled_Structure='stix:Observable[@id="%s"]'%obs.get_id(),
                Marking_Structure=[tlp]))

        package.get_Observables().add_Observable(obs)

import re
class Observable(Model, Relation):
    '''description of a cyber object'''

    plural = 'observables'
    unique = ('variety', 'value')

    variety = Member(Column(Enum(
                  *OBSERVABLE_VARIETY, name='OBSERVABLE_VARIETY'), 
                  nullable=False, index=True))
    value = Member(Column(String(50), nullable=False, index=True))
    value_text = Member(Column(Text))

    regex = {
        'ip':re.compile('(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\.|\[\.\])){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'),
        'url':re.compile('[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|DIAMONDS|TIPS|PHOTOGRAPHY|DIRECTORY|ENTERPRISES|KITCHEN|TODAY|PLUMBING|GRAPHICS|CONTRACTORS|GALLERY|SEXY|CONSTRUCTION|TATTOO|TECHNOLOGY|ESTATE|LAND|BIKE|VENTURES|CAMERA|CLOTHING|LIGHTING|SINGLES|VOYAGE|GURU|HOLDINGS|EQUIPMENT|BERLIN|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW|[0-9]{1,3})(?:\:\d+)?(?:\/\S?)',
                       re.IGNORECASE),
        'email':re.compile('.+\@.+\..+',
                         re.IGNORECASE),
        'domain':re.compile('[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|DIAMONDS|TIPS|PHOTOGRAPHY|DIRECTORY|ENTERPRISES|KITCHEN|TODAY|PLUMBING|GRAPHICS|CONTRACTORS|GALLERY|SEXY|CONSTRUCTION|TATTOO|TECHNOLOGY|ESTATE|LAND|BIKE|VENTURES|CAMERA|CLOTHING|LIGHTING|SINGLES|VOYAGE|GURU|HOLDINGS|EQUIPMENT|BERLIN|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW)',
                          re.IGNORECASE),
        'sha256':re.compile('(?:[A-F]|[0-9]){64}', re.IGNORECASE),
        'sha1':re.compile('(?:[A-F]|[0-9]){40}', re.IGNORECASE),
        'md5':re.compile('(?:[A-F]|[0-9]){32}', re.IGNORECASE)}

    protocol_regex = re.compile('[a-z]+://(.*)', re.IGNORECASE)

    def __init__(self, variety, value, value_text=None):

        if value_text is None and value is None:
            raise AttributeError('observable cannot have value: None')

        if value_text is None:
            value_text = value

        if variety:
            variety = variety.lower()
            if not variety in OBSERVABLE_VARIETY:
                raise AttributeError(
                    'observable variety %s not in enumerated set' % variety)

        #strip url protocol TODO: don't do this
        if variety == 'url':
          match = re.match(self.protocol_regex, value_text)
          if match:
              value_text = match.group(1)

        #regex check
        if variety in self.regex:
            if not re.match(self.regex[variety], value_text):
                raise AttributeError(
                    'observable value %s does not match regex for variety %s.'\
                    % (value_text, variety))

        self.variety = variety
        self.value = value[:50]
        self.value_text = value_text

    def dump_dict(self):
        '''override the default dict representation to ignore _text member'''

        #if self.value_text is not None:
        #    self.value = self.value_text

        d = {'variety':self.variety, 'value':self.value_text}

        return d

    #def dump_row(self, session):
    #    '''override default row repr to populate _text member'''
    #
    #    self.value_text = self.value
    #    self.value = self.value[:50]
    #    return Relation.dump_row(self, session)

    def dump_stix(self):
        obj = None
        if self.variety == 'ip':
            obj = mitre.Address(self.value, mitre.Address.CAT_IPV4)
        elif self.variety == 'network':
            obj = mitre.Address(self.value, mitre.Address.CAT_IPV4_NET)
        elif self.variety == 'url':
            obj = mitre.URI(self.value, mitre.URI.TYPE_URL)
        elif self.variety == 'asn':
            obj = mitre.Address(self.value, mitre.Address.CAT_ASN)
        elif self.variety == 'email':
            obj = mitre.Address(self.value, mitre.Address.CAT_EMAIL)
        elif self.variety == 'domain':
            obj = mitre.URI(self.value, mitre.URI.TYPE_DOMAIN)
        elif self.variety == 'sha256':
            obj = mitre.File()
            obj.add_hash(mitre.Hash(self.value, mitre.Hash.TYPE_SHA256))
        elif self.variety == 'sha1':
            obj = mitre.File()
            obj.add_hash(mitre.Hash(self.value, mitre.Hash.TYPE_SHA1))
        elif self.variety == 'md5':
            obj = mitre.File()
            obj.add_hash(mitre.Hash(self.value, mitre.Hash.TYPE_MD5))

        return mitre.Observable(obj)

class Document(Model, Relation):
    '''a document is a reference to the source material'''

    plural = 'documents'
    unique = ('name','received')

    name = Member(Column(String(100), nullable=False, index=True))
    name_text = Member(Column(Text))
    received = Member(Column(DateTime, nullable=False, index=True))
    source = Member(Column(String(50), index=True, nullable=False))
    category = Member(Column(Enum(*DOCUMENT_CATEGORY, 
                                  name='DOCUMENT_CATEGORY')))
    tlp = Member(Column(Enum(*TLP, name='TLP'), nullable=False))
    noforn = Member(Column(Boolean))
    nocomm = Member(Column(Boolean))
    investigator = Member(Column(String(50), index=True))
    text = Member(Column(Text))

    def __init__(self, name=None, category=None, source=None,
                 investigator=None, tlp='white', noforn=False, nocomm=False,
                 received=None, name_text=None, text=None):

        if received:
            self.received = received
        else:
            self.received = datetime.utcnow()

        if category:
            category = category.lower()
            if not category in DOCUMENT_CATEGORY:
                raise AttributeError(
                    'category %s not in enumerated set' % category)

        if tlp:
            tlp = tlp.lower()
            if not tlp in TLP:
                raise AttributeError(
                    'tlp %s not in enumerated set' % tlp)

        if not (name or category or source):
            raise AttributeError(
                'a document must have an name, category, or source')

        # derive document source and name
        if source is None:
            source = category
        if name is None:
            name = source

        if source == 'vzir' and not len(name) == 12:
            raise AttributeError(
                'a vzir source must have document name '+\
                'with a length of 12 characters.')

        if name_text is None:
            name_text = name
        self.name = name[:100]
        self.category = category
        self.source = source
        self.investigator = investigator
        self.tlp = tlp
        self.name_text = name_text
        self.noforn = noforn
        self.nocomm = nocomm
        self.text = text

    #TODO: implement an indexable text member and get rid of this
    def dump_dict(self):
        '''override the default dict representation to ignore _text member'''

        #if self.name_text is not None:
        #    self.name = self.name_text

        d = {'name':self.name_text, 'noforn':self.noforn,
             'nocomm':self.nocomm}
        for member_name in ('source', 'category', 'tlp',
                            'investigator', 'received'):
            value = getattr(self, member_name)
            if value is not None and not isinstance(value, Member):
                d[member_name] = value

        return d

    #def dump_row(self, session):
        #'''override default row repr to populate _text member'''

        #self.name_text = self.name
        #self.name = self.name[:100]
        #return Relation.dump_row(self, session)

    def dump_stix(self):
        '''ignores source and category. tlp and export handled by owner'''

        receivedtime = mitre.Time(received_time=self.received)
        #TODO: map db doc id to guid
        identity = mitre.Identity(name=self.name)
        source = mitre.InformationSource(time=receivedtime, identity=identity)
        source = source.to_obj()
        if self.investigator:
            contributor = mitre.Contributor()
            contributor.name = self.investigator
            source.set_Contributors(
                mitre.STIXPersonnelWrapper(contributor).to_obj())
        return source

    def dump_cybox(self):
        '''ignores source and category. tlp and export handled by owner'''

        receivedtime = mitre.Time(received_time=self.received)
        source = mitre.MeasureSource()
        source.time = receivedtime
        source.name = self.name
        if self.investigator:
            contributor = mitre.Contributor()
            contributor.name = self.investigator
            source.contributors = mitre.Personnel(contributor)
        return source.to_obj()

class TTP(Model, Relation):
    '''characterization of tactics, techniques, or procedures'''

    plural = 'ttps'

    malware = Member(Column(String(50), index=True))
    category = Member(Column(Enum(*TTP_CATEGORY, name='TTP_CATEGORY'), 
                             nullable=False, index=True))
    actions = Member(Column(Text))

    def __init__(self, category, actions=None, malware=None):

        #TODO: use sqla TypeDecorator to serialize actions list
        #http://docs.sqlalchemy.org/en/rel_0_8/core/types.html#sqlalchemy.types.TypeDecorator
        #http://docs.sqlalchemy.org/en/rel_0_8/core/types.html#typedecorator-recipes
        if isinstance(actions, list):
            actions = ';'.join(actions)

        if category:
            category = category.lower()
            if not category in TTP_CATEGORY:
                raise AttributeError(
                    'category %s not in enumerated set' % category)

        self.category = category
        self.actions = actions
        self.malware = malware

    def dump_stix(self):
        behavior = None
        if self.malware:
            #name = mitre.STIXStringObjectPropertyTypeWrapper(
            #           valueOf_=self.malware)
            malware = mitre.StructuredTextType(valueOf_=self.malware)
            behavior = mitre.BehaviorType(
                           Malware=mitre.MalwareType(
                               [mitre.MalwareInstanceType(
                                   Description=malware)]))

        category = mitre.StructuredTextType(valueOf_=self.category)
        resources = mitre.ResourceType(
                        Infrastructure=mitre.InfrastructureType(
                            Description=category))
        ttp = mitre.TTPType(Behavior=behavior, Resources=resources)

        return ttp

class Actor(Model, Relation):
    '''malicious agent'''

    plural = 'actors'
    unique = ('name',)

    name = Member(Column(String(50), nullable=False, index=True))

    def __init__(self, name):
        self.name = name

    def dump_stix(self, package):
        pass

class Campaign(Model, Relation):
    '''threat related by a common motive'''

    plural = 'campaigns'
    unique = ('name',)

    name = Member(Column(String(50), nullable=False, index=True))

    def __init__(self, name):
        self.name = name

    def dump_stix(self, package):
        pass

if __name__ == '__main__':
    import doctest
    doctest.testmod()
