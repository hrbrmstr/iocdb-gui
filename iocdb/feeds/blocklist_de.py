#!/usr/bin/env python

import re
import urllib2
import json
from datetime import datetime
import sys

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

ippat = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
ttp = TTP(category='malicious host')
feed_to_desc = {
    "http://www.blocklist.de/lists/ssh.txt":'SSH Brute Force Attack',
    "http://www.blocklist.de/lists/apache.txt":'Apache Attack',
    "http://www.blocklist.de/lists/asterisk.txt":'Asterisk Attack',
    "http://www.blocklist.de/lists/bots.txt":'Reported as a bot',
    "http://www.blocklist.de/lists/courierimap.txt":'Courier IMAP Targeting',
    "http://www.blocklist.de/lists/courierpop3.txt":'Courier POP3 Targeting',
    "http://www.blocklist.de/lists/email.txt":'Targeting Email services',
    "http://www.blocklist.de/lists/ftp.txt":'Targeting FTP services',
    "http://www.blocklist.de/lists/imap.txt":'Targeting IMAP',
    "http://www.blocklist.de/lists/ircbot.txt":'IRC Bot',
    "http://www.blocklist.de/lists/pop3.txt":'Targeting POP3',
    "http://www.blocklist.de/lists/postfix.txt":'Targeting postfix',
    "http://www.blocklist.de/lists/proftpd.txt":'Targeting ProFTPd',
    "http://www.blocklist.de/lists/sip.txt":'Targeting SIP'
}
        
@periodic_task(run_every=crontab(minute=0, hour=4), ignore_result=True)
def parse():
    valid = datetime.now()
    count = 0
    for feed, desc in feed_to_desc.items():
        page = urllib2.urlopen(feed)
        doc = Document(feed, 'osint', 'Blocklist.de Lists')
        iocdb.dispatcher.propagate(
            _get_rumors(valid, desc, doc, page), __file__)

def _get_rumors(valid, desc, doc, page):
    for line in page.readlines():
        line = line.strip('\n/.')
        if ippat.match(line):
            obs = Observable('ip', line)
            yield Rumor(obs, valid, doc, ttp, description=desc)
