from datetime import timedelta
#import logging

import celery
from celery.utils.log import get_task_logger
logging = get_task_logger(__file__)
import query_tools

import iocdb.dispatcher
import iocdb.config

args, arv, parser = iocdb.config.parse_config()
legacy = args.legacy_config.make()

def migrate(start_date, end_date):
    #for n in range(1, int((end_date - start_date).seconds)//3600+1):
    for n in range(1, int((end_date - start_date).days)*24+1):
        received_starting = end_date - timedelta(hours=n)
        received_ending = end_date - timedelta(hours=n-1)
        _migrate_range.delay(received_starting, received_ending)

@iocdb.dispatcher.app.task(ingnore_result=True)
def _migrate_range(received_starting, received_ending):
    logging.info('migrating documents received: {} -> {}'.format(
        received_starting, received_ending))
    range_criteria = (
        query_tools.Criteria(
            ('document', 'received'), received_starting, '>='),
        query_tools.Criteria(
            ('document', 'received'), received_ending, '<')
    )
    query = query_tools.Conjuction('and', range_criteria)
    with legacy.make_session() as session:
        iocdb.dispatcher.propagate(
            session.query(iocdb.model.Rumor, query), __file__, delay=False)
        #_propagate(session.query(iocdb.model.Association, query))

