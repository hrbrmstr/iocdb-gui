#!/usr/bin/env python

import json
import urllib2
from datetime import datetime

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

feed = "http://reputation.alienvault.com/reputation.data"
default_ttp = TTP('malicious host')
cat_to_ttp = {
    'Scanning Host':TTP('malicious host', 'hacking: footprinting'),
    'Malware Domain':TTP('malware distributor'),
    'Spamming':TTP('malicious host', 'social: spam'),
    'Malware IP':TTP('malware distributor'),
    'C&C':TTP('c2')
}

@periodic_task(run_every=crontab(minute=0, hour=4), ignore_result=True)
def parse():
    valid = datetime.now()
    doc = Document(
        name=feed, category='osint', source='AlienVault Reputation Data Feed')
    page = urllib2.urlopen(feed)
    iocdb.dispatcher.propagate(_get_rumors(page, valid, doc), __file__)

def test():
    #TODO: wrong
    valid = datetime.now()
    page = urllib2.urlopen(feed)
    for rumor in _get_rumors(page, valid):
        yield rumor

def _get_rumors(page, valid, doc):
    for line in page:
        fields = line.split('#')
        observable_value = fields[0]
        cat = fields[3]
        desc = '{} from {}, {}'.format(*fields[3:6])
        ttp = cat_to_ttp[cat] if cat in cat_to_ttp else default_ttp
        obs = Observable('ip', observable_value)
        yield Rumor(obs, valid, doc, ttp, description=desc)
