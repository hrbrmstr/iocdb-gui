import test_data.threat_intel
with default_sqlite.make_session() as session:
    session.add_all(test_data.threat_intel.rumors)
