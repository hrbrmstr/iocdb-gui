#!/usr/bin/env python

import re
import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ip = re.compile("[0-9]+(?:\.[0-9]+){3}")

ttp = TTP(category="malicious host", actions="hacking: brute force")

feeded = {"http://charles.the-haleys.org/ssh_dico_attack_hdeny_format.php/hostsdeny.txt": {'ttp':ttp, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='Charles Haley SSH Brute Force IPs')

		for line in page.readlines()[1::]:
			indicator = ip.findall(line)
			indicator = str(indicator).strip('\n[]\'')
			obs = Observable(feeddata['variety'], indicator) 

			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description='SSH Brute Forcer'))
	
	yield feed
