#!/usr/bin/env python

import json
import gzip
import urllib
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ttp = TTP(category="malicious host", actions='social: spam')

feeded = {"http://www.dnsbl.manitu.net/download/nixspam-ip.dump.gz": {'ttp':ttp, 'variety':'ip', 'desc':"NiXspam Blocklist Host"}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		urllib.urlretrieve(feedurl, "nixspam.gz")
		dump = gzip.open('nixspam.gz', 'rb')
		content = dump.readlines()
		
		doc = Document(name=feedurl, category='osint', tlp='white', source='NiX Spam Email DNSBL')
		
		for line in content:
			temp = line.split()
			obs = Observable(feeddata['variety'], temp[1]) 

			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=feeddata['desc']))
			
	yield feed
