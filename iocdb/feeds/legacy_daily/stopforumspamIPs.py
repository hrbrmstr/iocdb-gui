#!/usr/bin/env python

import zipfile
from urllib import urlopen
from datetime import datetime
from StringIO import StringIO
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

spam = TTP(category="malicious host", actions="social: spam")

feeded = {"http://www.stopforumspam.com/downloads/listed_ip_1_all.zip": {'ttp':spam, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		url = urlopen(feedurl)
		file = zipfile.ZipFile(StringIO(url.read()))

		doc = Document(name=feedurl, category='osint', tlp='white', source='Stop Forum Spam IP List')

		for name in file.namelist():
			for line in file.open(name).readlines():
				one = line.split(",")[0]
				two = line.split(",")[1]
        		
				obs = Observable(feeddata['variety'], one.strip('"'))
			
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description='Stop Forum Spam IP, seen ' + two.strip('"') + ' times.'))
				
	yield feed
