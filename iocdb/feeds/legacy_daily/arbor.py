#!/usr/bin/env python

import urllib2
import json
from datetime import datetime
import sys
from lxml import etree
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association


gen = TTP(category="malicious host")
dos = TTP(category="bot", actions='hacking: dos')
soc = TTP(category="malicious host", actions="social: phishing")
foot = TTP(category="malicious host", actions="hacking: footprinting")

feeded = {"http://atlas.arbor.net/srf/feeds/491883730/": {'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		tree = etree.parse(feedurl)
		root = tree.getroot()
		doc = Document(name=feedurl, category='osint', tlp='amber', source='Arbor Atlas Data Feed')

		for entry in root.findall('{http://www.w3.org/2005/Atom}entry'):
			ip = entry[6].text
			obs = Observable(feeddata['variety'], ip)
			
			attrib = entry[3].get('term')
			if attrib == 'attack':
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=gen, description ="Noted attack IP from Arbor Atlas"))
			elif attrib == 'conficker':		
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=gen, description ="Conficker infected host"))
			elif attrib == 'dos':		
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=dos, description ="Host noted in a DoS Attack by Arbor Atlas"))
			elif attrib == 'phish':		
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=soc, description ="Delivering or hosting Phishing attacks"))
			elif attrib == 'scan':		
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=foot, description ="Scanning host noted by Arbor Atlas"))
			
	yield feed