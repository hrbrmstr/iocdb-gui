#!/usr/bin/env python

import re
import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ignore = re.compile('^#')

ssh = TTP(category="malicious host", actions="hacking: brute force")
vnc = TTP(category="malicious host", actions="hacking: brute force")

textfeeds = {"https://dragonresearchgroup.org/insight/vncprobe.txt":
			{'ttp':vnc, 'variety':'ip', 'desc':'VNC Brute Forcing'},
			"https://dragonresearchgroup.org/insight/sshpwauth.txt":
			{'ttp':ssh, 'variety':'ip', 'desc':'SSH Brute Forcing'}}

def load(textfeeds=textfeeds, **query):
	feed = {'rumors' : []}
	valid = datetime.now()
	
	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='Dragon Research Group Reports')
						
		for line in page.readlines():
			if ignore.match(line):
				pass
			else:
				elems = line.split('|')
				obs = Observable(feeddata['variety'], elems[2].strip()) 

				feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=feeddata['desc']))
	
	yield feed
