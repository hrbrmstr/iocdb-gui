#!/usr/bin/env python

import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ttp = TTP(category="malicious host", actions="hacking: footprinting")

feeded = {"https://www.packetmail.net/iprep.txt": {'ttp':ttp, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='PacketMail IP Reputation List', nocomm=True)
		
		for line in page.readlines():
			if not line.startswith('#'):
				temp = line.split(';')
				
				#today = temp[1].strip().split(' ')[0].split('-')
				#ndate = datetime(int(today[0]), int(today[1]), int(today[2]))
				today = temp[1].strip().split(' ')[0]
				if today == str(datetime.today().date()):
					obs = Observable(feeddata['variety'], temp[0]) 

					feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=temp[2].strip()))
	
	yield feed