#!/usr/bin/env python

import re
import urllib2
import json
from datetime import datetime
import sys
import logging
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulStoneSoup
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

net = TTP(category="malware distributor")
mal = TTP(category="malware")

feeded = {"http://malc0de.com/rss/": {}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : [], 'associations' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		data = BeautifulStoneSoup(page)
		
		doc = Document(name=feedurl, category='osint', tlp='white', source='Malc0de')
		
		for entry in data.findAll('item'):

			url=str()
			ip=str()
			malz=str()
			elems = entry.find('description').text.split(',')
			
			for item in elems:
				if "IP Address:" in item:
					ip=item.split(":")[1].strip().encode('ascii','ignore')
				elif "URL: " in item:
					url=item.split(":")[1].strip().encode('ascii','ignore')
				elif "MD5: " in item:
					malz=item.split(":")[1].strip().encode('ascii','ignore')
			
			try:
				ipobs = Observable('ip', ip)
				feed['rumors'].append(Rumor(ipobs, valid, doc, ttp=net, description='Serving  Malware, see associations'))
			except:
				logging.debug("Couldn't build observable for %s" % ip)

			try:
				urlobs = Observable('url', url)
				feed['rumors'].append(Rumor(urlobs, valid, doc, ttp=net, description='Serving Malware, see associations'))
			except:
				logging.debug("Couldn't build observable for %s" % url)

			try:
				malobs = Observable('md5', malz)
				feed['rumors'].append(Rumor(malobs, valid, doc, ttp=mal, description='Unspecified malware, see associations'))
			except:
				logging.debug("Couldn't build observable for %s" % md5)
			
			try:
				ip2dom = Association(ipobs, urlobs, variety='resolved_to', valid=valid, document=doc)
				feed['associations'].append(ip2dom)
			except:
				logging.debug("Couldn't build malc0de ip2dom association for %s -> %s" % (ip,url))

			try:
				dom2ip = Association(urlobs, ipobs, variety='resolved_to', valid=valid, document=doc)
				feed['associations'].append(dom2ip)
			except:
				logging.debug("Couldn't build malc0de association for %s -> %s" % (url,ip))

			try:
				ip2mal = Association(ipobs, malobs, variety='downloaded', valid=valid, document=doc)
				feed['associations'].append(ip2mal)
			except:
				logging.debug("Couldn't build malc0de association for %s -> %s" % (ip,malz))

			try:
				mal2ip = Association(malobs, ipobs, variety='downloaded_from', valid=valid, document=doc)
				feed['associations'].append(mal2ip)
			except:
				logging.debug("Couldn't build malc0de association for %s -> %s" % (malz,ip))

			try:
				dom2mal = Association(urlobs, malobs, variety='downloaded', valid=valid, document=doc)
				feed['associations'].append(dom2mal)
			except:
				logging.debug("Couldn't build malc0de association for %s -> %s" % (url,malz))

			try:
				mal2dom = Association(malobs, urlobs, variety='downloaded_from', valid=valid, document=doc)
				feed['associations'].append(mal2dom)
			except:
				logging.debug("Couldn't build malc0de association for %s -> %s" % (malz,url))
			
			
	yield feed
