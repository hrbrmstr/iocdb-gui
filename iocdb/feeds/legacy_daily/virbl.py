#!/usr/bin/env python

import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ttp = TTP(category="malicious host")

feeded = {"http://virbl.org/download/virbl.dnsbl.bit.nl.txt": {'ttp':ttp, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='VIRBL IP List')
		
		for line in page.readlines()[1::]:
			obs = Observable(feeddata['variety'], line.rstrip()) 

			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description='Blacklisted IP from VIRBL'))
	
	yield feed
