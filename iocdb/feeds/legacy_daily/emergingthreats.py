#!/usr/bin/env python

import re
import urllib2
import json
import logging
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ignore = re.compile("^#")
ippat = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')

ttp = TTP(category='malicious host')

textfeeds = {
    "http://rules.emergingthreats.net/blockrules/compromised-ips.txt":
        {'ttp':ttp, 'variety':'ip', 'desc':'EmergingThreats Compromised IPs'},
    "http://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt":
        {'ttp':ttp, 'variety':'ip', 'desc':'Emerging Threats Firewall block list'}}
    
def load(textfeeds=textfeeds, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='Emerging Threats Lists')
		
		for line in page.readlines():
			if not ignore.match(line):
				line = line.strip('\n/.')
				obs=None
				if line.find("/") != -1:
					try:
						obs = Observable('network', line)
						feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=feeddata['desc']))
					except:
						logging.debug("Couldn't build observable for %s" % line)
						continue
				elif ippat.match(line):
					try:
						obs = Observable(feeddata['variety'], line)
						feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=feeddata['desc']))
					except:
						logging.debug("Couldn't build observable for %s" % line)
						continue

				
	yield feed
