#!/usr/bin/env python

import os
import re
import csv
import sys
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
import logging

dommatch = re.compile('[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW)',re.IGNORECASE)

target_dir = '/home/iocdb/incoming/CISCP'
target_files = []

# WARNING: if someone uploads multiple files, this gets all of them
os.chdir(target_dir)
for f in os.listdir("."):
    if f.startswith('CISCP Consolidated Indicators') and f.endswith('.csv'):
        target_files.append(f)

def load(textfeeds='', **query):
    feed = {'rumors' : []}
    valid = datetime.now()

    for target in target_files:
        filename = target_dir + '/' + target
        with open(filename,'rb') as f:
            reader = csv.reader(f)
            indicators = []
        
            for row in reader:
                if row[0] != 'INDICATOR_VALUE':
                    try:
                        temp = row[0].decode("utf8")
                        indicators.append({'content':row[0],'type':row[1],'comment':row[2],'role':row[3],'attack':row[4],'observed':row[5],'handling':row[6],'product':row[7]})
                    except:
                        pass
    
        for i in indicators:
            try:
                if i['type'] in ['FILENAME','REGKEY','STRING']:
                    continue
                elif i['role'] == 'SUBJECT':
                    continue
                
                if len(i['content']) > 255:
                    continue
                
                i['content'] = i['content'].replace('[[.]]','.').replace('[.]','.').replace('[dot]','.').replace('[@]','@').replace('hxxp://', '').replace('hXXp://','').replace('hxxps://', '').replace('hXXps://', '')
                i['comment'] = i['comment'].replace('\n',' ')
            
                if i['content'][-1] == ' ':
                    i['content'] = i['content'][:-1]
                if i['handling'] == 'TLP:GREEN':
                    i['handling'] = 'green'
                elif i['handling'] == 'TLP:AMBER':
                    i['handling'] = 'amber'
                elif i['handling'] == 'TLP:AMBER FOUO':
                    i['handling'] = 'amber'
                else:
                    sys.stderr.write('WARNING: Skipped line with unknown TLP:')
                    sys.stderr.write(str(i))
            
                if i['type'] == 'IPV4ADDR':
                    if i['role'] == 'C2':
                        i['category'] = 'c2'
                    else:
                        i['category'] = 'malicious host'
                    i['iocdb_type'] = 'ip'
                elif i['type'] == 'IPV4CIDR':
                    if i['role'] == 'C2':
                        i['category'] = 'c2'
                    else:
                        i['category'] = 'malicious host'
                    i['iocdb_type'] = 'network'
                elif i['type'] == 'EMAIL':
                    i['category'] = 'malicious email address'
                    i['iocdb_type'] = 'email'
                elif i['type'] == 'FQDN':
                    if i['role'] == 'C2':
                        i['category'] = 'c2'
                    else:
                        i['category'] = 'malicious host'
                    i['iocdb_type'] = 'domain'
                elif i['type'] == 'URL':
                    if i['role'] == 'C2':
                        i['category'] = 'c2'
                    else:
                        i['category'] = 'malicious host'
                    if dommatch.match(i['content']) and \
                        dommatch.match(i['content']).group(0) == i['content']:
                            i['iocdb_type'] = 'domain'
                    else:
                        i['iocdb_type'] = 'url'
                elif i['type'] == 'MD5':
                    i['category'] = 'malware'
                    i['iocdb_type'] = 'md5'
                elif i['type'] == 'SHA1':
                    i['category'] = 'malware'
                    i['iocdb_type'] = 'sha1'
            
                if i['attack'] == 'C2':
                    i['actions'] = 'hacking: backdoor or c2'
                else:
                    i['actions'] = ''
                
                if not i['observed']:
                    fulldate = i['product'].split('-')[1]
                    i['observed'] = datetime(int(fulldate[:4]),int(fulldate[4:6]),int(fulldate[6:8]))
                else:
                    fulldate = i['observed'].split('-')
                    i['observed'] = datetime(int(fulldate[0]),int(fulldate[1]),int(fulldate[2]))
                
                description_pieces = []
                if i['comment']:
                    description_pieces.append('Comment= '+ i['comment'])
                if i['role']:
                    description_pieces.append('Role= '+i['role'])
                if i['attack']:
                    description_pieces.append('AttackPhase= '+i['attack'])
                description_pieces.append('Product= '+i['product'])
                i['description'] = '; '.join(description_pieces)
                i['description'] = i['description'].decode("ascii",'ignore')
            
                i['description'] = i['description'].replace('"',"'").replace('\n','-')
            
                if i['actions']:
                    ttp = TTP(category=i['category'], actions=i['actions'])
                else:
                    ttp = TTP(category=i['category'])
                
                doc = Document(name=target, category='csint', tlp=i['handling'], source='CISCP Consolidated Indicators')
                obs = None
                try:
                    obs = Observable(i['iocdb_type'], i['content']) 
                except:
                    logging.exception('ciscp: could not parse indicator')
                    continue
                feed['rumors'].append(Rumor(obs, i['observed'], doc, ttp=ttp, description=i['description']))
            except:
                logging.exception('ciscp: unhandled error')
                continue

    for target in target_files:
        os.remove(target_dir+'/'+target)            

    yield feed
