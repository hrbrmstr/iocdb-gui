#!/usr/bin/env python

import sys
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulSoup
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
from datetime import datetime
import iocdb.feeds.legacylibs.lib.mechanize as mechanize
import json
import re


naughty = TTP(category="malicious host")

feeded = {"http://twitter.com/naughty_ips": {'ttp':naughty, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		browser = mechanize.Browser()
		browser.addheaders = [('User-agent', 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)')]
		report = browser.open(feedurl)
		html = report.read()
		page = BeautifulSoup(html)

		doc = Document(name=feedurl, category='osint', tlp='white', source='@Naughty_IPs Twitter Feed')

		for entry in page.findAll("p" , {"class" : "js-tweet-text tweet-text"}):
			ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', entry.text)
			for hit in ip:
				obs = Observable(feeddata['variety'], hit)
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description ="Naughty IP"))

	yield feed
