#!/usr/bin/env python

import re
import sys
import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulSoup
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

delete = re.compile("    <description/>")

spam = TTP(category="malicious host", actions="social: spam")
hbf = TTP(category="malicious host", actions="hacking: brute force")
ttp = TTP(category="malicious host")

feeded = {"http://www.projecthoneypot.org/list_of_ips.php?by=21&rss=1": {'ttp':spam, 'variety':'ip', 'desc':'Comment / Forum Spammer '},
"http://www.projecthoneypot.org/list_of_ips.php?by=3&rss=1":{'ttp':ttp, 'variety':'ip', 'desc':'Bad Host '},
"http://www.projecthoneypot.org/list_of_ips.php?by=18&rss=1":{'ttp':hbf, 'variety':'ip', 'desc':'Brute Force Attacker '},
"http://www.projecthoneypot.org/list_of_ips.php?by=15&rss=1":{'ttp':spam, 'variety':'ip', 'desc':'Email Spammer '}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		fixed = ""
		for line in page.readlines():
			if delete.match(line):
				pass
			else:
				fixed += line
				
		data = BeautifulSoup(fixed)
		
		doc = Document(name=feedurl, category='osint', tlp='white', source='Project Honeypot')
		
		if feeddata['ttp'] == hbf:
			one = 8
			two = 5
		else:
			one = 7
			two = 4
		
		for entry in data.findAll('item'):
			ind = entry.find('title').text.split()
			desc = entry.find('description').text.split()
			
			obs = Observable(feeddata['variety'], ind[0])
			
			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=feeddata['desc'] + 'first seen ' + desc[one] + ' and seen ' + desc[two] + ' times'))
			
	yield feed
