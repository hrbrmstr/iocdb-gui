#!/usr/bin/env python

import sys
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulSoup
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
from datetime import datetime
import iocdb.feeds.legacylibs.lib.mechanize as mechanize
import json

dist = TTP(category="malware distributor")
mal = TTP(category="malware")

feeded = {"http://vxvault.siri-urz.net/ViriList.php?s=1&m=150": {}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : [], 'associations' : []}
	valid = datetime.now()
	
	for feedurl, feeddata in textfeeds.items():
		browser = mechanize.Browser()
		report = browser.open(feedurl)
		html = report.read()
		page = BeautifulSoup(html)
		
		table = page.find("div", {"id" : "page"}).findNext("table")
		
		doc = Document(name=feedurl, category='osint', tlp='white', source='VxVault Malware Data')
		
		for entry in table.findAll("tr")[1::]:
			temp = []
			for elem in entry.findAll("td"):
				temp.append(elem.text)
			
			ipobs = Observable('ip', temp[3].rstrip()) 
			urlobs = Observable('url', temp[1].lstrip('[D] '))
			malobs = Observable('md5', temp[2])
			
			feed['rumors'].append(Rumor(ipobs, valid, doc, ttp=dist, description='Malicious Hosting, see associations'))
			feed['rumors'].append(Rumor(urlobs, valid, doc, ttp=dist, description='Malicious Serving, see associations'))
			feed['rumors'].append(Rumor(malobs, valid, doc, ttp=mal, description='Malware, see associations'))
			
			ip2dom = Association(ipobs, urlobs, variety='resolved_to', valid=valid, document=doc)
			dom2ip = Association(urlobs, ipobs, variety='resolved_to', valid=valid, document=doc)
			ip2mal = Association(ipobs, malobs, variety='downloaded', valid=valid, document=doc)
			mal2ip = Association(malobs, ipobs, variety='downloaded_from', valid=valid, document=doc)
			dom2mal = Association(urlobs, malobs, variety='downloaded', valid=valid, document=doc)
			mal2dom = Association(malobs, urlobs, variety='downloaded_from', valid=valid, document=doc)
			
			feed['associations'].append(ip2dom)
			feed['associations'].append(dom2ip)
			feed['associations'].append(ip2mal)
			feed['associations'].append(mal2ip)
			feed['associations'].append(dom2mal)
			feed['associations'].append(mal2dom)
			
	yield feed
