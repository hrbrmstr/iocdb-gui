#!/usr/bin/env python

import re
import json
import urllib2
import glob
import shutil
import time
import logging
import os
import sys
from datetime import datetime,date

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

doc_valid=datetime.now()
doc_name="file-name"
doc_tlp="amber"
doc_source="Support-Intelligence URL Feed"
description="Support-Intelligence URL Feed Indicator:"
doc_category="csint"
target_dir='/var/data/support-intelligence/url/'
archive_dir='/var/data/support-intelligence/url-archive/'
target_files= []
domain_regex= re.compile('[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKc2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKc2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZc2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW)',re.IGNORECASE)
ip_regex = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
feed_regex = re.compile('^http',re.IGNORECASE)
dom_regex = re.compile('^\ \ \ \ \ ',re.IGNORECASE)
url_feed_data=dict()
domain_categories={}

def get_threatcat(id):
    
    input=int(id)
    threat_categories=[]
    actions=[]
    original_desc=[]

    association={}
    classifications = {
        0 : 'malicious host', #::Spam
        1 : 'bot', #::BOT member
        2 : 'proxy', #::Open Proxy
        3 : 'proxy', #::Open Relay
        4 : 'malicious host', #::Insecure Web Server
        5 : 'c2', #::IRC Abuse
        6 : 'c2', #::Fast Flux
        7 : 'proxy', #::Tor Exit Node
        8 : 'malicious host', #::bogon
        9 : 'malicious host', #::Dynamic IP
        10 : 'malicious host', #::Malware Host
        11 : 'malicious host', #::Infrastructure
        12 : 'malicious host', #::Ddos
        13 : 'malicious host', #::Port Scanning
        14 : 'c2' #::c2 / C&C Server
        }

    support_intel_desc = {
        0 : 'Spam',
        1 : 'Bot member',
        2 : 'Open Proxy',
        3 : 'Open Relay',
        4 : 'Insecure Web Server',
        5 : 'IRC Abuse',
        6 : 'Fast Flux',
        7 : 'Tor Exit Node',
        8 : 'Bogon',
        9 : 'Dynamic IP',
        10 : 'Malware Host',
        11 : 'Infrastructure',
        12 : 'DDoS',
        13 : 'Port Scanning',
        14 : 'C2 / C&C Server'
    }

    action_enumerations = {
        0 : 'social: spam',
        1 : 'malware: unknown',
        2 : '',
        3 : '',
        4 : 'malware: exploit vuln',
        5 : 'malware: c2',
        6 : 'malware: c2',
        7 : '',
        8 : '',
        9 : '',
        10 : 'malware: c2',
        11 : 'malware: client-side',
        12 : 'malware: dos',
        13 : 'malware: scan network',
        14 : 'malware: c2'
        }

    for position in classifications:
        if input & (1<<position):
            threat_categories.append(classifications[position])
            actions.append(action_enumerations[position])
            original_desc.append(support_intel_desc[position])

    if len(threat_categories) > 1:
        #association['malicious host'] = 'other'
        return ['malicious host','other']
    else:
        #association[threat_categories[0]] = actions[0]
        return [threat_categories[0],actions[0],original_desc[0]]

@periodic_task(run_every=crontab(minute=0, hour=4), ignore_result=True)
def parse():

    results=int()

    _load_categories()
    #::Parse feed data::#
    for ip_feed in glob.glob(os.path.join(target_dir, "uri-*")):
        doc_doc=Document(name=ip_feed, category=doc_category, tlp=doc_tlp, source=doc_source)
        doc_valid=datetime.strptime(ip_feed.split("/")[-1].split("-")[1], '%Y%m%d%H')
        results += _propagate_rumors(ip_feed,doc_valid,doc_doc)
        shutil.move(ip_feed,archive_dir)
    
    print('Support Intelligence URL feed propagated {} rumors'.format(results))

def _load_categories():

    for domain_feed in glob.glob(os.path.join(target_dir, "dom-uri_*")):
        for line in open(domain_feed):
            data=line.strip('\n')
            if dom_regex.match(data):
                feed_data=data.split()
                domain_categories[feed_data[1]] = feed_data[0]
        shutil.move(domain_feed,archive_dir)

def _propagate_rumors(page, valid, document, page_size=10000):

    rumors = []
    associations = []
    count = 0

    for line in open(page):
        if len(rumors) == page_size:
            iocdb.dispatcher.propagate(rumors)
            count += page_size
            rumors = []
        elif len(associations) == page_size:
            iocdb.dispatcher.propagate(associations)
            associations=[]
        else:
            data=line.strip('\n')
            if feed_regex.match(data):
                feed_data=data.split('\t')
                if domain_regex.match(feed_data[1]):
                    domain=feed_data[1]
                    try:
                        observable_domain=Observable('domain',domain)
                    except:
			print (" 1. Couldn't build observable for %s" % domain)
                        logging.debug("Couldn't build observable for %s" % domain)
                        continue
                    classification=[]
                    if domain in domain_categories:
			try:
				classification=get_threatcat(domain_categories[domain])
				description="Support-Intelligence URL Feed Indicator: " + classification[2]
				observable_ttp = TTP(category=classification[0],actions=classification[1])
				rumors.append(Rumor(observable_domain,valid,document,ttp=observable_ttp,description=description))
                        except :
			    print (" 2. Couldn't build observable for %s - %s" % (domain,sys.exc_info()[0]))
                            logging.debug("Could not build rumor for: %s from file: %s" % (domain,page))
			    continue
                    else:
			try:
				classification=['malicious host','unknown']
				description="Support-Intelligence URL Feed Indicator: " + classification[1]
				observable_ttp = TTP(category=classification[0],actions=classification[1])
				rumors.append(Rumor(observable_domain,valid,document,ttp=observable_ttp,description=description))
                        except:
	                    print ("3. Couldn't build observable for %s" % domain)
                            logging.debug("Could not build rumor for: %s from file: %s" % (domain,page))
			    continue
                    if feed_data[2] != "NULL" and feed_data[2] != "NXDOMAIN":
                        if feed_data[2].find(',') != -1:
                            for associated_ip in feed_data[2].split(","):
				try:
                                	observable_association_ip=Observable('ip',associated_ip)
					observable_ttp = TTP(category=classification[0],actions=classification[1])
					ip_association=Association(observable_association_ip, observable_domain, variety='resolved_to', valid=doc_valid, document=document)
					associations.append(ip_association)
                                except:
		                    print ("4. Couldn't build observable for %s" % domain)
                                    logging.debug("Could not build rumor for: %s from file: %s" % (domain,page))
				    continue
                        else:
                            try:
                                observable_association_ip=Observable('ip',feed_data[2])
                                ip_association=Association(observable_association_ip, observable_domain, variety='resolved_to', valid=doc_valid, document=document)
                                associations.append(ip_association)
                            except:
				print ("5. Couldn't build observable for %s" % domain)
                                logging.debug("Could not build rumor for: %s from file: %s" % (domain,page))
				continue
                elif ip_regex.match(feed_data[1]):
                    try:
                        classification=['malicious host','unknown']
                        ip=feed_data[1]
                        observable_ip=Observable('ip',ip)
                        description="Support-Intelligence URL Feed Indicator: " + classification[1]
                        observable_ttp = TTP(category=classification[0],actions=classification[1])
                        rumors.append(Rumor(observable_ip,doc_valid,document,ttp=observable_ttp,description=description))
                    except:
			print ("6. Couldn't build observable for %s" % domain)
                        logging.debug("Could not build rumor for: %s from file: %s" % (domain,page))
			continue
    iocdb.dispatcher.propagate(rumors)
    iocdb.dispatcher.propagate(associations)
    count += len(rumors)
    return count
