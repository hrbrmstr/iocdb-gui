'''
an adapter for the legacy feed spec
'''

import sys

from celery.schedules import crontab
from celery.task import periodic_task
from celery import group
import celery.utils.log

import iocdb.mappings.legacy
import iocdb.model
import iocdb.dispatcher
import iocdb.feeds.legacy_hourly
import iocdb.feeds.legacy_daily

task_logging = celery.utils.log.get_task_logger(__file__)

@periodic_task(run_every=crontab(minute=0, hour=4), ignore_result=True)
def legacy_daily():
    group(
        _load_legacy.s('iocdb.feeds.legacy_daily.%s' % (module_name)) 
        for module_name in iocdb.feeds.legacy_daily.__all__
    ).delay()

@periodic_task(run_every=crontab(minute=40), ignore_result=True)
def legacy_hourly():
    group(
        _load_legacy.s('iocdb.feeds.legacy_hourly.%s' % (module_name)) 
        for module_name in iocdb.feeds.legacy_hourly.__all__
    ).delay()

@iocdb.dispatcher.app.task(ignore_result=True)
def _load_legacy(module_path):
    task_logging.info('initializing legacy module {}'.format(module_path))
    module = _import(module_path)
    for page in module.load():
        if 'rumors' in page:
            task_logging.info('mapping a page of {} rumors from legacy module {}'.format(len(page['rumors']), module_path))
            rumors = _get_model_objects(
                page['rumors'], iocdb.mappings.legacy.rumor_mapping)
            iocdb.dispatcher.propagate(rumors, module_path)
        if 'associations' in page:
            task_logging.info('mapping a page of {} associations from legacy module {}'.format(len(page['associations']), module_path))
            associations = _get_model_objects(
                page['associations'],
                iocdb.mappings.legacy.association_mapping)
            iocdb.dispatcher.propagate(associations, module_path)
        
def _get_model_objects(legacy_objects, mapping):
    for obj in legacy_objects:
        yield mapping.map(obj)

def _import(module_path):
    __import__(module_path)
    module = sys.modules[module_path]
    return module
