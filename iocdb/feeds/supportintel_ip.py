#!/usr/bin/env python

import re
import json
import urllib2
import glob
import shutil
import time
import logging
import os
import sys
from datetime import datetime,date

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

doc_valid=datetime.now()
doc_name="file-name"
doc_tlp="amber"
doc_source="Support-Intelligence Feed"
description="Support-Intelligence Feed Indicator"
doc_category="csint"
target_dir='/var/data/support-intelligence/ip/'
archive_dir='/var/data/support-intelligence/ip-archive/'
target_files= []
regex = re.compile('^[0-9]*\/[0-9]{1,2}\t[0-9]*',re.IGNORECASE)

def get_threatcat(id):

    input=int(id)
    threat_categories=[]
    actions=[]
    original_desc=[]

    association={}
    classifications = {
        0 : 'malicious host', #::Spam
        1 : 'bot', #::BOT member
        2 : 'proxy', #::Open Proxy
        3 : 'proxy', #::Open Relay
        4 : 'malicious host', #::Insecure Web Server
        5 : 'c2', #::IRC Abuse
        6 : 'c2', #::Fast Flux
        7 : 'proxy', #::Tor Exit Node
        8 : 'malicious host', #::bogon
        9 : 'malicious host', #::Dynamic IP
        10 : 'malicious host', #::Malware Host
        11 : 'malicious host', #::Infrastructure
        12 : 'malicious host', #::Ddos
        13 : 'malicious host', #::Port Scanning
        14 : 'c2' #::c2 / C&C Server
        }

    support_intel_desc = {
        0 : 'Spam',
        1 : 'Bot member',
        2 : 'Open Proxy',
        3 : 'Open Relay',
        4 : 'Insecure Web Server',
        5 : 'IRC Abuse',
        6 : 'Fast Flux',
        7 : 'Tor Exit Node',
        8 : 'Bogon',
        9 : 'Dynamic IP',
        10 : 'Malware Host',
        11 : 'Infrastructure',
        12 : 'DDoS',
        13 : 'Port Scanning',
        14 : 'C2 / C&C Server'
    }

    action_enumerations = {
        0 : 'social: spam',
        1 : 'malware: unknown',
        2 : '',
        3 : '',
        4 : 'malware: exploit vuln',
        5 : 'malware: c2',
        6 : 'malware: c2',
        7 : '',
        8 : '',
        9 : '',
        10 : 'malware: c2',
        11 : 'malware: client-side',
        12 : 'malware: dos',
        13 : 'malware: scan network',
        14 : 'malware: c2'
        }

    for position in classifications:
        if input & (1<<position):
            threat_categories.append(classifications[position])
            actions.append(action_enumerations[position])
            original_desc.append(support_intel_desc[position])

    if len(threat_categories) > 1:
        #association['malicious host'] = 'other'
        return ['malicious host','other','malicious host']
    else:
        #association[threat_categories[0]] = actions[0]
        return [threat_categories[0],actions[0],original_desc[0]]

    #return association

def short2long(ip):
    short_ip=int(ip)
    oct1=short_ip/(256 ** 3)
    oct2=((short_ip%(256 ** 3)/(256 ** 2)))
    oct3=(short_ip%(256 **3))%(256 **2)/(256**1)
    oct4=(short_ip%(256**3))%(256**2)%(256**1)/(256**0)

    dotted_ip="%d.%d.%d.%d" % (oct1,oct2,oct3,oct4)

    return dotted_ip    

def list_ips(cidr_net):
    ip_addresses=[]
    ip_cidr=IPNetwork(cidr_net)
    for i in range(len(ip_cidr)):
        ip_addresses.append(ip_cidr[i])

    return ip_addresses
    
@periodic_task(run_every=crontab(minute=0, hour=4), ignore_result=True)
def parse():

    results=int()

    #::Parse feed data::#
    for ip_feed in glob.glob(os.path.join(target_dir, "*.dat")):
        doc_doc=Document(name=ip_feed, category=doc_category, tlp=doc_tlp, source=doc_source)
        doc_valid=datetime.strptime(ip_feed.split("/")[-1].split(".")[0], '%Y%m%d')
        results += _propagate_rumors(ip_feed,doc_valid,doc_doc)
        shutil.move(ip_feed,archive_dir)
    
    print('Support Intelligence IP feed propagated {} rumors'.format(results))

def _propagate_rumors(page, valid, document, page_size=10000):

    rumors = []
    count = 0

    for line in open(page):
        if len(rumors) == page_size:
            iocdb.dispatcher.propagate(rumors)
            count += page_size
            rumors = []
        else:
            data=line.strip('\n')
            if regex.match(data):
                feed_data=data.split('\t')
                ip_data=feed_data[0].split('/')
                ip=short2long(ip_data[0])
                if str(ip_data[1]) == str(32):
                    classification=get_threatcat(feed_data[1])
                    description="Support-Intelligence IP Feed Indicator: %s " % (classification[2])
                    try:
                        observable_ip = Observable('ip',ip)
                    except:
                        logging.debug("Couldn't build observable for %s" % ip)
                        continue
                    observable_ttp = TTP(category=classification[0],actions=classification[1])
                    rumors.append(Rumor(observable_ip,valid,document,ttp=observable_ttp,description=description))
           	else:
			classification=get_threatcat(feed_data[1])
			description="Support-Intelligence IP Feed Indicator: %s " % (classification[2])
			ips=list_ips(str(ip) + "/" + str(ip_data[1]))
                	if isinstance(ips,list):
                    		for ip_found in ips:
                        		try:
                            			observable_ip = Observable('ip',ip_found)
                        		except:
                            			logging.debug("Couldn't build observable for %s" % ip)
                            			continue
                        		observable_ttp = TTP(category=classification[0],actions=classification[1])
                        		rumors.append(Rumor(observable_ip,valid,document,ttp=observable_ttp,description=description))

    iocdb.dispatcher.propagate(rumors)
    count += len(rumors)
    return count
