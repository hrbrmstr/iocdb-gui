#!/usr/bin/env python

from urllib import urlopen
from datetime import datetime
import logging
import re
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ttp = TTP(category="c2")

feeded = {"http://osint.bambenekconsulting.com/feeds/cl-iplist.txt": {'ttp':ttp, 'variety':'ip'}, "http://osint.bambenekconsulting.com/feeds/cl-domlist.txt": {'ttp':ttp, 'variety':'domain'}}
ignore = re.compile("^#")

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		result = urlopen(feedurl)
		data = result.read()
		doc = Document(name=feedurl, category='osint', tlp='white', source='Bambenek Consulting')
		
		for line in data:
			if not ignore.match(line):
				fields = line.split(',')
				
				try:
					obs = Observable(feeddata['variety'], fields[0])
					feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=str(fields[1])+' - Active non-sinkholed'))
				except:
					logging.debug("Couldn't build observable for %s" % fields[0])

	yield feed
