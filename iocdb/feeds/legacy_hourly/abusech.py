#!/usr/bin/env python
'''abuse.ch decoder load method yields rumors and associations from
abuse.ch feeds. only returns results that did not occur in the last pull.
'''

import traceback
import re
import urllib2
from datetime import datetime
from time import sleep
import sys

from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulStoneSoup
from iocdb.feeds.legacylibs.lib.utils import Diff

zeus = TTP(malware='zeus', category='c2',
           actions=['Hacking: Backdoor or C2'])
spyeye = TTP(malware='spyeye', category='c2',
             actions=['Hacking: Backdoor or C2'])
palevo = TTP(malware='palevo', category='c2',
             actions=['Hacking: Backdoor or C2'])
feodo = TTP(malware='cridex', category='c2',actions=['Hacking:Backdoor or C2'])

textfeeds = {
    "https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist":
        {'ttp':zeus, 'variety':'domain', 'offset':6,
         'start_text': '##############################################################################\n# abuse.ch ZeuS domain blocklist'},
    "https://zeustracker.abuse.ch/blocklist.php?download=ipblocklist":
        {'ttp':zeus, 'variety':'ip', 'offset':6,
         'start_text': '##############################################################################\n# abuse.ch ZeuS IP blocklist'},
    "https://spyeyetracker.abuse.ch/blocklist.php?download=ipblocklist":
        {'ttp':spyeye, 'variety':'ip', 'offset':6,
         'start_text': '################################################################################\n# abuse.ch SpyEye IP blocklist'},
    "https://spyeyetracker.abuse.ch/blocklist.php?download=domainblocklist":
        {'ttp':spyeye, 'variety':'domain', 'offset':6,
         'start_text': '################################################################################\n# abuse.ch SpyEye domain blocklist'},
    "https://palevotracker.abuse.ch/blocklists.php?download=ipblocklist":
        {'ttp':palevo, 'variety':'ip', 'offset':1,
         'start_text': '# Palevo C&C IP Blocklist by abuse.ch'},
    "https://palevotracker.abuse.ch/blocklists.php?download=domainblocklist":
        {'ttp':palevo, 'variety':'domain', 'offset':1,
         'start_text': '# Palevo C&C Domain Blocklist by abuse.ch'},
    "https://https://zeustracker.abuse.ch/blocklist.php?download=baddomains":
        {'ttp':zeus, 'variety':'domain', 'offset':1,
         'start_text': '############################################################################\n# abuse.ch ZeuS domain blocklist'},
    "https://zeustracker.abuse.ch/blocklist.php?download=badips":
        {'ttp':zeus, 'variety':'ip', 'offset':1,
         'start_text': '#############################################################################################\n# abuse.ch ZeuS IP blocklist'},
    "https://feodotracker.abuse.ch/blocklist.php?download=ipblocklist":
         {'ttp':feodo, 'variety':'ip', 'offset':6,
          'start_text':'#############################################################################\n# Feodo IP Blocklist'},
    "https://feodotracker.abuse.ch/blocklist.php?download=domainblocklist":
    	 {'ttp':feodo, 'variety':'domain', 'offset':6,
    	  'start_text':'#############################################################################\n# Feodo Domain Blocklist'},
    "https://feodotracker.abuse.ch/blocklist.php?download=badips":
    	 {'ttp':feodo, 'variety':'ip', 'offset':6,
    	  'start_text':'#############################################################################\n# Feodo BadIP Blocklist (BadIPs)'}}

rssfeeds = {
    "https://palevotracker.abuse.ch/?rssfeed":
        {'ttp':palevo, 'variety':'domain'}}

def _batchdownload(*feedlists):
    '''try three times to download all feeds
       sleep 10 seconds after each iteration'''
    for i in range(3):
        fail = False

        if i:
            sleep(10)

        for feedlist in feedlists:
            for feedurl, feeddata in feedlist.items():
                if 'text' not in feeddata:
                    try:
                        feeddata['text'] = urllib2.urlopen(feedurl).read()
                    except:
                        #traceback.print_exc()
                        fail = True
                        continue

                    if 'start_text' in feeddata:
                        #make sure what we got actually looks like
                        #the right list
                        if feeddata['text'].find(feeddata['start_text']) != 0:
                            fail = True

        if not fail:
            break


def load(textfeeds=textfeeds, rssfeeds=rssfeeds,
         batchdownload=_batchdownload, **query):
    feed = {'rumors':[], 'associations':[]}
    valid = datetime.now()
    batchdownload(textfeeds, rssfeeds)

    #parse text feeds
    #each line in text feed relates observable to a threat
    for feedurl, feeddata in textfeeds.items():

        if 'text' not in feeddata: #download failed
            continue

        try:
            #feeddiff = Diff(feedurl)
            feeddiff = {}
            doc = Document(name=feedurl, category='osint',
                           source='abuse.ch')

            for line in feeddata['text'].split('\n')[feeddata['offset']::]:
                value = line.strip('\n/.')
                try:
                    feeddiff[value] = Observable(feeddata['variety'], value)
                except AttributeError: #Observable rejected by model
                    continue #pass regex mismatch

            for obs in feeddiff.values():
                try:
                    feed['rumors'].append(
                        Rumor(obs, valid, doc, ttp=feeddata['ttp']))
                except AttributeError: #Rumor rejected by model
                    #TODO: proper logging
                    traceback.print_exc()

        except AttributeError: #Document rejected by model
            #TODO: proper logging
            traceback.print_exc()

    #parse rss feed
    #each rss entry relates a domain to a threat and a domain to an ip
    for rssurl, rssdata in rssfeeds.items():

        if 'text' not in rssdata: #download failed
            continue

        #feeddiff = Diff(rssurl)
        feeddiff = {}
        dom = BeautifulStoneSoup(rssdata['text'])
        try:
            doc = Document(
                 #url parsed from entry 404's - discarded
                 rssurl, category='osint', source='abuse.ch')

            for entry in dom.findAll('item'):

                try:
                    title = entry.find('title').text
                    value, validstr = title.split()
                    valid = datetime.strptime(validstr, '%Y-%m-%d')
                    obs = None
                    try:
                      obs = Observable(rssdata['variety'], value)
                    except AttributeError:
                      continue #regex mismatch
                    description = entry.find('description').text
                    ipvalue = str(re.findall(r'[0-9]+(?:\.[0-9]+){3}', description))\
                                  .strip('[]u\'')
                    ip = None
                    try:
                      ip = Observable('ip', ipvalue)
                    except AttributeError:
                      continue #regex mismatch

                    rumor = Rumor(obs, valid, doc, ttp=rssdata['ttp'],
                                  description=description)
                    association = Association(obs, ip, valid, doc,
                                              variety='resolves to')
                    feeddiff[title] = (rumor, association)
                except AttributeError: #obs, rumor, or assoc rejected by model
                    #TODO: proper logging
                    traceback.print_exc()

            for rumor, association in feeddiff.values():
                feed['rumors'].append(rumor)
                feed['associations'].append(association)

        except AttributeError: #Document rejected by model
            #TODO: proper logging
            traceback.print_exc()

    #TODO: proper logging (this will be difficult to do without updating the feed to the new spec)
    print("abuse.ch produced {} legacy rumors and {} legacy associations".format(len(feed['rumors']), len(feed['associations'])))
    yield feed

def clear():
    '''clear diff index for all urls'''
    for url in textfeeds:
        try:
            Diff(url).clear()
        except OSError:
            pass #file dne
    for url in rssfeeds:
        try:
            Diff(url).clear()
        except OSError:
            pass #file dne

from unittest import TestCase, TestSuite
class TEST_Abusech(TestCase):

    textinput = '# ignore\n1.1.1.1\n2.2.2.2'
    rssinput = '<rss><item>' +\
                   '<title>3dmodelsart.com 2013-03-26</title>' +\
                   '<description>' +\
                   '216.146.143.197, SBL: Not listed, Status: online' +\
                   '</description>' +\
               '</item></rss>'
    textfeeds = {'textinput':{'ttp':zeus, 'variety':'ip', 'offset':1}}
    rssfeeds = {'rssinput':{'ttp':palevo, 'variety':'domain'}}

    @staticmethod
    def batchdownload(*feedlists):
        for feedlist in feedlists:
            for url, data in feedlist.items():
                data['text'] = getattr(TEST_Abusech, url)

    def test_load(self):
        data = load(TEST_Abusech.textfeeds,
                    TEST_Abusech.rssfeeds,
                    TEST_Abusech.batchdownload).next()
        self.assertEqual(data['associations'][0].left.value,
                         '3dmodelsart.com')
        self.assertEqual(len(data['rumors']), 3)

    def tearDown(self):
        try:
            Diff('textinput').clear()
        except OSError:
            pass
        try:
            Diff('rssinput').clear()
        except OSError:
            pass

class TEST_Functional(TestCase):

    def test_functional(self):
        from sys import stdout
        from lib.encoding import JSONEncoder
        JSONEncoder(stdout, indent=2).dump(load().next())

        #passes if no error. user verification of output required

    def tearDown(self):
        #clear()
        pass

def load_tests(loader, tests, pattern):
    suite = TestSuite()
    suite.addTests(loader.loadTestsFromTestCase(TEST_Abusech))
    return suite
