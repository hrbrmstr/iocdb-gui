#!/usr/bin/env python

import requests
import calendar
import time
import os
import re
from datetime import datetime, timedelta
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

urlmatch = re.compile('[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW|[0-9]{1,3})(?:\/\S+)',re.IGNORECASE)

ttp = TTP(category="malicious host")
feeded = {"https://www.virustotal.com/vtapi/v2/url/distribution": {'ttp':ttp, 'variety':'url'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : [], 'associations' : []}

	lastpull = ((calendar.timegm(time.gmtime()) - 1 * 3600) * 1000) 
	for feedurl, feeddata in textfeeds.items():
		params = {'apikey': '1f575a54bb21ed2d5d774e851435f59ec7f0c1dc75dc8b23315137fbb4dc83a3', 'reports': 'true', 'limit':10000, 'after': lastpull}
		response = requests.get(feedurl, params=params)
		response_json = response.json()
		
		for url in response_json:
			if url['response_code'] == 1:
				sub = url['url']
				if sub.startswith('http://'):
					sub = sub[7:]
				elif sub.startswith('https://'):
					sub = sub[8:]

				if urlmatch.match(sub):
					src = url['permalink']
					doc = Document(name=src, category='osint', tlp='white', source='VirusTotal URL Feed')
				
					pos = str(url['positives'])
					totes = str(url['total'])	
					date = url['scan_date']
                                        date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
				
					desc = ''
					for scan in url['scans']:
						if 'detected' in url['scans'][scan]:
							if url['scans'][scan]['detected'] == True:
								full = '%s - %s; ' % (scan, url['scans'][scan]['result'])
								desc = desc + full
					scr = 'Score: ' + str(url['score'])
					desc = desc + scr
				
					obs = Observable(feeddata['variety'], sub)
					feed['rumors'].append(Rumor(obs, date, doc, ttp=feeddata['ttp'], description=desc))
				
					if 'Response content SHA-256' in url['additional_info']:
						urlobs = Observable('url', sub)
						malobs = Observable('sha256', url['additional_info']['Response content SHA-256'])
				
						url2mal = Association(urlobs, malobs, variety='downloaded_file', valid=date, document=doc)
						feed['associations'].append(url2mal)

	yield feed
