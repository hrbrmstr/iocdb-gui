#!/usr/bin/env python
'''
Extract data from a query repo using provided query args and encode results.
'''

import argparse
import datetime

import query_tools

import iocdb.config
import iocdb.model

def main():
    args = _parse_args()
    with args.repository.make_session() as repository_session:
        results = repository_session.query(args.model, args.criteria)
        with args.encoder.make_session() as encoder_session:
            encoder_session.add_all(results)

def _parse_args():
    '''
    initialize repository, query model, query criteria and encoder as
    args.repository, args.model, args.criteria, and args.encoder
    '''
    args, argv, config_parser = iocdb.config.parse_config()
    args, argv, logging_parser = iocdb.config.parse_logging(argv, args)
    args, argv, repository_parser = iocdb.config.parse_session_manager(
        argv, args, 'repository', default='sqlite')
    args, argv, encoder_parser = iocdb.config.parse_session_manager(
        argv, args, 'encoder', default='json')
    args, argv, query_parser = _parse_query_args(argv, args)
    help_parser = argparse.ArgumentParser(
        description=__doc__,
        parents=[config_parser, logging_parser, repository_parser,
                 encoder_parser, query_parser])
    #remaining args should be empty
    remaining_args = help_parser.parse_args(argv) 
    return args

def _parse_query_args(argv, args):
    '''
    initialize query model and criteria as args.model and args.criteria
    '''
    parser = _make_query_parser()
    #must subscript argv so that it is treated like sys.argv
    #(ignore the executable name at argv[0])
    args, argv = parser.parse_known_args(argv[1:], args)
    args.model = _get_model_from_arg(args.model)
    args.criteria = _get_criteria_from_args(args)
    return args, argv, parser

def _make_query_parser():
    parser = argparse.ArgumentParser(add_help=False)
    group = parser.add_argument_group(
        'query args',
        'valid DATETIME formats are yyyy-mm-dd or yyyy-mm-ddThh:mm:ss')
    group.add_argument(
        'model', default='rumors', choices=['rumors','associations'],
        nargs='?', help='name of model to query')
    group.add_argument(
        '--observable_varieties', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--observable_values', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--observable_file', type=argparse.FileType(), metavar='FILE',
        help='query newline seperated observable values found in this file')
    group.add_argument(
        '--observable_whitelist', type=argparse.FileType(), metavar='FILE', 
        help='ignore newline seperated observable values found in this file')
    group.add_argument(
        '--document_sources', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--document_names', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--document_investigators', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--valid_starting', nargs='?', 
        type=_cli_datetime, metavar='DATETIME', help='(inclusive)')
    group.add_argument(
        '--valid_ending', nargs='?', type=_cli_datetime, metavar='DATETIME',
        help='(exclusive)')
    group.add_argument(
        '--received_starting', nargs='?', 
        type=_cli_datetime, metavar='DATETIME', help='(inclusive)')
    group.add_argument(
        '--received_ending', nargs='?', type=_cli_datetime, metavar='DATETIME',
        help='(exclusive)')
    group.add_argument(
        '--actor_names', type=unicode,  nargs='*', metavar='TEXT')
    group.add_argument(
        '--campaign_names', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--ttp_malware', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--ttp_categories', type=unicode, nargs='*', metavar='TEXT')
    return parser

def _cli_datetime(arg):
    try:
        return datetime.datetime.strptime(arg, '%Y-%m-%d')
    except ValueError:
        return datetime.datetime.strptime(arg, '%Y-%m-%dT%H:%M:%S')

def _get_model_from_arg(arg):
    if arg == 'rumors':
        return iocdb.model.Rumor
    elif arg == 'associations':
        return iocdb.model.Association

def _get_criteria_from_args(args):
    criteria = []
    if args.observable_varieties is not None:
        criteria.append(query_tools.Criteria(
            ('observable', 'variety'), args.observable_varieties))
    observable_values = []
    if args.observable_values is not None:
        observable_values.extend(args.observable_values)
    if args.observable_file is not None:
        observable_values.extend(unicode(val.strip()) 
                                 for val in args.observable_file)
    if len(observable_values) > 0:
        criteria.append(query_tools.Criteria(
            ('observable', 'value'), value=observable_values))
    if args.observable_whitelist is not None:
        observable_whitelist = [unicode(val.strip())
                                for val in args.observable_whitelist]
        criteria.append(query_tools.Criteria(
            ('observable', 'value'), operator='not in', 
            value=observable_whitelist))
    if args.document_sources is not None:
        criteria.append(query_tools.Criteria(
            ('document', 'source'), value=args.document_sources))
    if args.document_names is not None:
        criteria.append(query_tools.Criteria(
            ('document', 'name'), value=args.document_names))
    if args.document_investigators is not None:
        criteria.append(query_tools.Criteria(
            ('document', 'investigator'), value=args.document_investigators))
    if args.valid_starting is not None:
        criteria.append(query_tools.Criteria(
            ('valid',), operator='>=', value=args.valid_starting))
    if args.valid_ending is not None:
        criteria.append(query_tools.Criteria(
            ('valid',), operator='<', value=args.valid_ending))
    if args.received_starting is not None:
        criteria.append(query_tools.Criteria(
            ('document', 'received'), operator='>=', 
            value=args.received_starting))
    if args.received_ending is not None:
        criteria.append(query_tools.Criteria(
            ('document', 'received'), operator='<', 
            value=args.received_ending))
    if args.actor_names is not None:
        criteria.append(query_tools.Criteria(
            ('actor', 'name'), value=args.actor_names))
    if args.campaign_names is not None:
        criteria.append(query_tools.Criteria(
            ('campaign', 'name'), value=args.campaign_names))
    if args.ttp_malware is not None:
        criteria.append(query_tools.Criteria(
            ('ttp', 'malware'), value=args.ttp_malware))
    if args.ttp_categories is not None:
        criteria.append(query_tools.Criteria(
            ('ttp', 'category'), value=args.ttp_categories))
    return query_tools.Conjuction('and', criteria)

if __name__ == '__main__':
    main()
