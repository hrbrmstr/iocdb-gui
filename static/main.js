/**
 * Created by tim on 8/5/14.
 */


var lstMalCat = [
    { id: ' ', text: ' ' },
    { id: 'malicious host' , text: 'malicious host' },
    { id: 'bot' , text: 'bot' },
    { id: 'exfiltration site', text: 'exfiltration site' },
    { id: 'c2', text: 'c2' },
    { id: 'proxy', text: 'proxy' },
    { id: 'malware distributor', text: 'malware distributor' },
    { id: 'exploit server', text: 'exploit server' },
    { id: 'malware', text: 'malware' },
    { id: 'sinkhole', text: 'sinkhole' },
    { id: 'malicious email address' , text: 'malicious email address' }

];

var lstConnectors = [
    { id: 'connects' , text: 'connects to' },
    { id: 'resolves' , text: 'resolves to' }
];

var lstDocCat = [
    { id: ' ', text: ' ' },
    { id: 'ciscp' , text: 'ciscp' },
    { id: 'csint', text: 'csint' },
    { id: 'govint', text: 'govint' },
    { id: 'ids2', text: 'ids2' },
    { id: 'osint' , text: 'osint' },
    { id: 'vecirt' , text: 'vecirt' },
    { id: 'vzir', text: 'vzir' },
    { id: 'atims', text: 'atims' }
];


var lstAction = [
    { id: ' ', text: ' ' },
    { id: 'hacking', text: 'hacking' },
    { id: 'malware', text: 'malware' },
    { id: 'misuse', text: 'misuse' },
    { id: 'physical', text: 'physical' },
    { id: 'social', text: 'social' }
];


var lstHackVar = [
    { id: ' ', text: ' ' },
    { id: 'Abuse of functionality', text: 'Abuse of functionality' },
    { id: 'Brute force', text: 'Brute force' },
    { id: 'Buffer overflow', text: 'Buffer overflow' },
    { id: 'Cache poisoning', text: 'Cache poisoning' },
    { id: 'Session prediction', text: 'Session prediction' },
    { id: 'CSRF', text: 'CSRF' },
    { id: 'XSS', text: 'XSS' },
    { id: 'Cryptanalysis', text: 'Cryptanalysis' },
    { id: 'DoS', text: 'DoS' },
    { id: 'Footprinting', text: 'Footprinting' },
    { id: 'Forced browsing', text: 'Forced browsing' },
    { id: 'Format string attack', text: 'Format string attack' },
    { id: 'Fuzz testing', text: 'Fuzz testing' },
    { id: 'HTTP request smuggling', text: 'HTTP request smuggling' },
    { id: 'HTTP request smuggling', text: 'HTTP request smuggling' },
    { id: 'HTTP response smuggling', text: 'HTTP response smuggling' },
    { id: 'HTTP Response Splitting', text: 'HTTP Response Splitting' },
    { id: 'Integer overflows', text: 'Integer overflows' },
    { id: 'LDAP injection', text: 'LDAP injection' },
    { id: 'Mail command injection', text: 'Mail command injection' },
    { id: 'MitM', text: 'MitM' },
    { id: 'Null byte injection', text: 'Null byte injection' },
    { id: 'Offline cracking', text: 'Offline cracking' },
    { id: 'OS commanding', text: 'OS commanding' },
    { id: 'Path traversal', text: 'Path traversal' },
    { id: 'RFI', text: 'RFI' },
    { id: 'Reverse engineering', text: 'Reverse engineering' },
    { id: 'Routing detour', text: 'Routing detour' },
    { id: 'Session fixation', text: 'Session fixation' },
    { id: 'Session replay', text: 'Session replay' },
    { id: 'Soap array abuse', text: 'Soap array abuse' },
    { id: 'Special element injection', text: 'Special element injection' },
    { id: 'SQLi', text: 'SQLi' },
    { id: 'SSI injection', text: 'SSI injection' },
    { id: 'URL redirector abuse', text: 'URL redirector abuse' },
    { id: 'Use of backdoor or C2', text: 'Use of backdoor or C2' },
    { id: 'Use of stolen creds', text: 'Use of stolen creds' },
    { id: 'XML attribute blowup', text: 'XML attribute blowup' },
    { id: 'XML entity expansion', text: 'XML entity expansion' },
    { id: 'XML external entities', text: 'XML external entities' },
    { id: 'XML injection', text: 'XML injection' },
    { id: 'XPath injection', text: 'XPath injection' },
    { id: 'XQuery injection', text: 'XQuery injection' },
    { id: 'Virtual machine escape', text: 'Virtual machine escape' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstHackVect = [
    { id: ' ', text: ' ' },
    { id: '3rd party desktop', text: '3rd party desktop' },
    { id: 'Backdoor or C2', text: 'Backdoor or C2' },
    { id: 'Desktop sharing', text: 'Desktop sharing' },
    { id: 'Physical access', text: 'Physical access' },
    { id: 'Command shell', text: 'Command shell' },
    { id: 'Partner', text: 'Partner' },
    { id: 'VPN', text: 'VPN' },
    { id: 'Web application', text: 'Web application' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstMalVar = [
    { id: ' ', text: ' ' },
    { id: 'Adware', text: 'Adware' },
    { id: 'Backdoor', text: 'Backdoor' },
    { id: 'Brute force', text: 'Brute force' },
    { id: 'Capture app data', text: 'Capture app data' },
    { id: 'Capture stored data' , text: 'Capture stored data' },
    { id: 'Client-side attack', text: 'Client-side attack' },
    { id: 'Click fraud', text: 'Click fraud' },
    { id: 'C2', text: 'C2' },
    { id: 'Destroy data', text: 'Destroy data' },
    { id: 'Disable controls', text: 'Disable controls' },
    { id: 'DoS', text: 'DoS' },
    { id: 'Downloader', text: 'Downloader' },
    { id: 'Exploit vuln', text: 'Exploit vuln' },
    { id: 'Export data', text: 'Export data' },
    { id: 'Packet sniffer', text: 'Packet sniffer' },
    { id: 'Password dumper', text: 'Password dumper' },
    { id: 'Ram scraper' , text: 'Ram scraper' },
    { id: 'Ransomware', text: 'Ransomware' },
    { id: 'Rootkit', text: 'Rootkit' },
    { id: 'Scan network', text: 'Scan network' },
    { id: 'Spam', text: 'Spam' },
    { id: 'Spyware/Keylogger', text: 'Spyware/Keylogger' },
    { id: 'SQL injection', text: 'SQL injection' },
    { id: 'Adminware', text: 'Adminware' },
    { id: 'Worm', text: 'Worm' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstMalVect = [
    { id: ' ', text: ' ' },
    { id: 'Direct install', text: 'Direct install' },
    { id: 'Download by malware', text: 'Download by malware' },
    { id: 'Email autoexecute', text: 'Email autoexecute' },
    { id: 'Email link', text: 'Email link' },
    { id: 'Email attachment', text: 'Email attachment' },
    { id: 'Instant messaging', text: 'Instant messaging' },
    { id: 'Network propagation', text: 'Network propagation' },
    { id: 'Remote injection', text: 'Remote injection' },
    { id: 'Removable media', text: 'Removable media' },
    { id: 'Web drive-by', text: 'Web drive-by' },
    { id: 'Web download', text: 'Web download' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstMisVar = [
    { id: ' ', text: ' ' },
    { id: 'Knowledge abuse', text: 'Knowledge abuse' },
    { id: 'Privilege abuse', text: 'Privilege abuse' },
    { id: 'Embezzlement', text: 'Embezzlement' },
    { id: 'Data mishandling', text: 'Data mishandling' },
    { id: 'Email misuse', text: 'Email misuse' },
    { id: 'Net misuse', text: 'Net misuse' },
    { id: 'Illicit content', text: 'Illicit content' },
    { id: 'Unapproved workaround', text: 'Unapproved workaround' },
    { id: 'Unapproved hardware', text: 'Unapproved hardware' },
    { id: 'Unapproved software', text: 'Unapproved software' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' },
];

var lstMisVect = [
    { id: ' ', text: ' ' },
    { id: 'Physical access', text: 'Physical access' },
    { id: 'LAN access', text: 'LAN access' },
    { id: 'Embezzlement', text: 'Embezzlement' },
    { id: 'Remote access', text: 'Remote access' },
    { id: 'Non-corporate', text: 'Non-corporate' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstPhysLoc = [
    { id: ' ', text: ' ' },
    { id: 'Partner facility', text: 'Partner facility' },
    { id: 'Partner vehicle', text: 'Partner vehicle' },
    { id: 'Personal residence', text: 'Personal residence' },
    { id: 'Personal vehicle', text: 'Personal vehicle' },
    { id: 'Public facility', text: 'Public facility' },
    { id: 'Public vehicle', text: 'Public vehicle' },
    { id: 'Victim secure area', text: 'Victim secure area' },
    { id: 'Victim work area', text: 'Victim work area' },
    { id: 'Victim public area', text: 'Victim public area' },
    { id: 'Victim grounds', text: 'Victim grounds' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstPhysVar = [
    { id: ' ', text: ' ' },
    { id: 'Assault', text: 'Assault' },
    { id: 'Sabotage', text: 'Sabotage' },
    { id: 'Snooping', text: 'Snooping' },
    { id: 'Surveillance', text: 'Surveillance' },
    { id: 'Tampering', text: 'Tampering' },
    { id: 'Theft', text: 'Theft' },
    { id: 'Wiretapping', text: 'Wiretapping' },
    { id: 'Connection', text: 'Connection' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstPhysVect = [
    { id: ' ', text: ' ' },
    { id: 'Privileged access', text: 'Privileged access' },
    { id: 'Visitor privileges', text: 'Visitor privileges' },
    { id: 'Bypassed controls', text: 'Bypassed controls' },
    { id: 'Disabled controls', text: 'Disabled controls' },
    { id: 'Uncontrolled location', text: 'Uncontrolled location' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstSocTar = [
    { id: ' ', text: ' ' },
    { id: 'Auditor', text: 'Auditor' },
    { id: 'Call center', text: 'Call center' },
    { id: 'Cashier', text: 'Cashier' },
    { id: 'Customer', text: 'Customer' },
    { id: 'End-user', text: 'End-user' },
    { id: 'Executive', text: 'Executive' },
    { id: 'Finance', text: 'Finance' },
    { id: 'Former employee', text: 'Former employee' },
    { id: 'Helpdesk', text: 'Helpdesk' },
    { id: 'Human resources', text: 'Human resources' },
    { id: 'Maintenance', text: 'Maintenance' },
    { id: 'Manager', text: 'Manager' },
    { id: 'Partner', text: 'Partner' },
    { id: 'Guard', text: 'Guard' },
    { id: 'Developer', text: 'Developer' },
    { id: 'System admin', text: 'System admin' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstSocVar = [
    { id: ' ', text: ' ' },
    { id: 'Baiting', text: 'Baiting' },
    { id: 'Bribery', text: 'Bribery' },
    { id: 'Elicitation', text: 'Elicitation' },
    { id: 'Extortion' , text: 'Extortion' },
    { id: 'Forgery', text: 'Forgery' },
    { id: 'Influence', text: 'Influence' },
    { id: 'Scam', text: 'Scam' },
    { id: 'Phishing', text: 'Phishing' },
    { id: 'Pretexting', text: 'Pretexting' },
    { id: 'Propaganda', text: 'Propaganda' },
    { id: 'Spam', text: 'Spam' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];

var lstSocVect = [
    { id: ' ', text: ' ' },
    { id: 'Documents', text: 'Documents' },
    { id: 'Email', text: 'Email' },
    { id: 'In-person', text: 'In-person' },
    { id: 'IM', text: 'IM' },
    { id: 'Phone', text: 'Phone' },
    { id: 'Removable media', text: 'Removable media' },
    { id: 'SMS', text: 'SMS' },
    { id: 'Social media', text: 'Social media' },
    { id: 'Software', text: 'Software' },
    { id: 'Website', text: 'Website' },
    { id: 'Unknown', text: 'Unknown' },
    { id: 'Other', text: 'Other' }
];


var lstTlp = [
    { id: 'white', text: 'white' },
    { id: 'green', text: 'green' },
    { id: 'amber', text: 'amber' },
    { id: 'red', text: 'red' }
];


var regEmail = /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW)/gmi;
var regUrl = /[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW|[0-9]{1,3})(?:\/\S*)/gmi;
var regIP = /(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\.|\[\.\])){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/gmi;
var regDomain = /([A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW))(?:\s|$)/gmi;
var regSHA256 = /(?:[A-F]|[0-9]){64}/gmi;
var regSHA1 = /(?:[A-F]|[0-9]){40}/gmi;
var regMD5 = /(?:[A-F]|[0-9]){32}/gmi;


var colHeaders = "observable,valid,description,category,malware,action,variety,vector,actor,campaign";

function parseDoc(){
    var data = document.getElementById("txtData").value.replace(/\n/gmi," ");
    var arObserv = [];
    var res = '';
    //need to go in this order
    res = data.match(regUrl);
    if(res != null){
        for(i=0;i<res.length;i++){
            arObserv.push(res[i]);
        }
        data = data.replace(regUrl,'');
    }

    res = data.match(regIP);
    if(res != null){
        for(i=0;i<res.length;i++){
            arObserv.push(res[i]);
        }
        data = data.replace(regIP,'');
    }


    res = data.match(regEmail);
    if(res != null){
        for(i=0;i<res.length;i++){
            arObserv.push(res[i]);
        }
        data = data.replace(regEmail,'');

    }

    res = data.match(regDomain);
    if(res != null){
        for(i=0;i<res.length;i++){
            arObserv.push(res[i]);
        }
        data = data.replace(regDomain,'');
    }


    res = data.match(regSHA256);
    if(res != null){
        for(i=0;i<res.length;i++){
            arObserv.push(res[i]);
        }
        data = data.replace(regSHA256,'');

    }

    res = data.match(regSHA1);
    if(res != null){
        for(i=0;i<res.length;i++){
            arObserv.push(res[i]);
        }
        data = data.replace(regSHA1,'');
    }


    res = data.match(regMD5);
    if(res != null){
        for(i=0;i<res.length;i++){
            arObserv.push(res[i]);
        }
    }


    var rowID = 0;
    var strRec = '';
    //before we add, need to reset the grid
    w2ui.grid2.reset();
    for(i=0;i<arObserv.length;i++){
        rowID += 1;
        strRec = {recid: rowID, observable: arObserv[i]};
        w2ui.grid2.records.push(strRec);
    }
    w2ui.grid2.refresh();
}

function getFieldType(fieldValue){
    var res = '';
    res = fieldValue.match(regUrl);
    if(res != null){
       return 'url';
    }

    res = fieldValue.match(regIP);
    if(res != null){
           return 'ip';
    }


    res = fieldValue.match(regEmail);
    if(res != null){
        return  'email';

    }

    res = fieldValue.match(regDomain);
    if(res != null){
        return 'domain';
    }


    res = fieldValue.match(regSHA256);
    if(res != null){
        return 'sha256';

    }

    res = fieldValue.match(regSHA1);
    if(res != null){
        return 'sha1';
    }


    res = fieldValue.match(regMD5);
    if(res != null){
        return 'md5';
    }

    return null;
}

function myFunction() {
    document.getElementById('txtData').value = "";
    var fileList = $('#txtFile').data('selected');
    document.getElementById('txtData').value = w2utils.base64decode(fileList[0].content);
}

function importData(){
    var data = document.getElementById("txtData").value;
    //check to see if there's data
    if(data.trim() !== ''){
        //split the txtData into array
        //var rows = data.split('\n');
       /* var cols = rows.shift().split(',');
        if(cols.length == 1){
            cols = rows.shift().split("\t");
        }*/
        //check for the right column headers
        var colErr = '';
        /*for(i=0;i<cols.length;i++){
            if(colHeaders.search(cols[i]) < 0 ){
                colErr += "," + cols[i];
            }
        }*/
        if(colErr === ''){
            //load the main grid
            var myData = Papa.parse(data,{header: true}).data;

            var rowID = w2ui.grid2.records.length;
            for(i=0;i<myData.length;i++) {
                rowID += 1;

                var myRec = $.extend({}, {recid: rowID}, myData[i]);
                w2ui.grid2.records.push(myRec);

                //validate the date needs to be in a function
                /*var rowStyle = {};

                var objRecord = '';
                var colIdx = '';
                $.each(cols, function (idx, val) {
                    objRecord = w2ui.grid2.get(rowID);
                    colIdx = w2ui.grid2.getColumn(val,true);
                    var dtCurrent = new Date();
                    //rowStyle[colIdx] = '';
                    if (val === 'valid') {
                        var tmpObj = {};
                        tmpObj[colIdx] = 'color: red;';
                        var dtCell = new Date(w2ui.grid2.parseField(objRecord, val));
                        if (!w2utils.isDate(dtCell)) {
                            //check to see if it's a future data
                            $.extend(rowStyle,tmpObj);
                            console.log("valid date not properly formatted");
                        }
                        else{
                            //check to see it's not in the future
                            if(dtCell.getFullYear() > dtCurrent.getFullYear()){
                                $.extend(rowStyle,tmpObj);
                                console.log("valid date is set for a future date");
                            }
                        }
                    }


                });
                w2ui.grid2.get(rowID).style = rowStyle;
                rowStyle = {};*/

            }

            w2ui.grid2.refresh();
        }
        else{
            w2alert("Bad Column Names: " + colErr);
        }

    }
    else{
        w2alert("D'OH! No Data!");
    }

}